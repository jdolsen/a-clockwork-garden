//#include <motor293.h>
#include <Arduino.h>
#include <Keypad.h>
#include <RFM69registers.h>
#include <SPI.h>
#include <RFM69.h>
#include <EEPROMIO.h>
#include <CommandHandler.h>
#include <EventQueue.h>
#include <RFM69NodeSimple.h>
#include <SPIFlash.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <MemoryFree.h>

#define SECOND 1000

#define AUTO_DISABLED 0
#define AUTO_ENABLED 1
#define AUTO_TIMED 2

#define NUM_TO_SAVE 54
#define ICON_SIZE 32

#define FONT_NUM 10
#define FONT_SIZE 6
#define FONT_OFFSET NUM_TO_SAVE*ICON_SIZE
#define SMALL_FONT_OFFSET (NUM_TO_SAVE*ICON_SIZE)+(FONT_NUM*FONT_SIZE)
#define FONT_NORMAL 0
#define FONT_THIN 1

#ifdef __AVR_ATmega1284P__
  #define LED           15 // Moteino MEGAs have LEDs on D15
  #define FLASH_SS      23 // and FLASH SS on D23
#else
  #define LED           9 // Moteinos have LEDs on D9
  #define FLASH_SS      8 // and FLASH SS on D8
#endif

// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out    (CLK) //(SCLK)
// pin 6 - Serial data out     (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select     (CE) //(CS)
// pin 3 - LCD reset (RST)
//Adafruit_PCD8544 display = Adafruit_PCD8544(19,18,17,16,15);//(7, 6, 5, 4, 3); 84x48
Adafruit_PCD8544 display = Adafruit_PCD8544(19,18,17,16);//16 is RST, CE/CS is grounded

int doorStates[4];
int stateHours[4];
int stateMinutes[4];
int ambLight = 0;
int lightState = 0;
int dayState=0;

int feedLevel=0;
int waterLevel=0;
int coopTemp=0;
int heatingState=0;

const byte ROWS = 4; //four rows
const byte COLS = 3; //four columns
char hexaKeys[ROWS][COLS] = {
  {'1','2','3'},//,'A'},
  {'4','5','6'},//,'B'},
  {'7','8','9'},//,'C'},
  {'*','0','#'}//,'D'}
};

byte colPins[COLS] = {7,14,15};//8,9};//,14};
byte rowPins[ROWS] = {3,4,5,6};
char inKey;
char menu=0;
char cursorLine=0;
char menuState=0;

char doorNumberOffset = 10;
char dayStateOffset = 20;
char errorOffset = 26;
char warningOffset = 35;
char xOffset = 36;
char noOffset = 37;
char lightStateOffset = 38;
char ambientLightOffset= 40;

char autoOpenOptionsOffset = 41;
//char autoCloseOptionsOffset = 43;//not needed due to display layout

char autoDoorSelectOffset = 47;

char currentTimeOffset = 49;
char closeTimeOffset = 50;
char openTimeOffset = 51;
char closeLightLevelOffset = 52;
char openLightLevelOffset = 53;
/*char* states[11]={
    "Init",         //0
    "Clsd",       //1
    "Opn St",   //2 
    "Unlk",     //3
    "Opnng",      //4
    "Clsng",      //5
    "Cls Sp",//6
    "Opn Sp", //7
    "Opn",         //8
    "Cls St",  //9
    ""};   //10
*/    
/*char* errors[9]={
    "Opn Er",  //0
    "Cls Er",  //1
    "Opn TO", //2
    "Cls TO", //3
    "Opn IO", //4
    "Cls IO", //5
    "Mtr Ov", //6
    "Mtr Un", //7
    "",//No error     //8
};*/

#define NO_ERROR_NUM 8

char buttonPin1;
char buttonReleased1;

char buttonPin2;
char buttonReleased2;
unsigned long lastRequest = 0;
unsigned long statusTimeout = 0;
char hours = 0;
char minutes = 0;
char seconds = 0;
unsigned long lastTime = 0;

int autoOpenThreshold=0;
int autoOpenTime = 0;
int autoOpenEnableType=AUTO_DISABLED;
int autoOpenTimeHours=0;
int autoOpenTimeMinutes=0;
char autoOpenEventTrigger=0;

int autoCloseThreshold=0;
int autoCloseTime = 0;
int autoCloseEnableType=AUTO_DISABLED;
int autoCloseTimeHours=0;
int autoCloseTimeMinutes=0;
char autoCloseEventTrigger=0;

int autoDoor=0;

int setTimeHours=0;
int setTimeMinutes=0;

unsigned int offsets[4];
SPIFlash flash(FLASH_SS, 0xEF30);

//=========================================================================

void setup(){
  flash.initialize();
  offsets[0] = EEPROMIO.loadInit(autoOpenTimeHours);
  offsets[1] = EEPROMIO.loadInit(autoOpenTimeMinutes); 
  offsets[2] = EEPROMIO.loadInit(autoCloseTimeHours); 
  offsets[3] = EEPROMIO.loadInit(autoCloseTimeMinutes);
  
  node.nodeInit("COOPABCDCOOPABCD", 10, 2, RF69_915MHZ);
  
  node.handler.registerCommandHandler(
  "stat", 
   stateUpdate, 
  "");
  
   node.handler.registerCommandHandler(
  "atp", 
   autoStateUpdate, 
  "");
  
  lastRequest = millis();
  statusTimeout = millis();
  lastTime = millis();
  node.queue.addQueueEvent(requestStatus,0,0);
  node.queue.addQueueEvent(sendAutoParams,1000,3,4,0,0); 
  display.begin();
  display.clearDisplay();
  display.setContrast(50);
  display.setTextSize(1);
  display.setTextColor(BLACK);
  //display.setRotation(2);
  display.setTextWrap(false);
}

//=========================================================================

void loop(){
  node.nodeLoop(300);
  updateTime();

  //request state and error info every 10 minutes
  if(millis()-lastRequest > 600000){//1000ms * 60sec * 10min ){
     lastRequest = millis();
     //display.clearDisplay();
     //display.println("Req");
     //display.display();
     //delay(1000);
     node.queue.addQueueEvent(requestStatus,0,0); 
  }
  
  if(millis() - statusTimeout > 1000 * 60){
    //TODO: signal an error if no messages received in 30 minutes
  }
  
  //Trigger any timed events that have expired
  if(autoOpenEnableType==AUTO_TIMED && autoOpenEventTrigger==0){
    if(hours==autoOpenTimeHours && minutes==autoOpenTimeMinutes){
      //attempt to open the door
      node.queue.addQueueEvent(transitionState,0,2,autoDoor,1);  
      autoOpenEventTrigger=1; 
    }
  }
  
  if(autoCloseEnableType==AUTO_TIMED && autoCloseEventTrigger==0){
    //attempt to close the door
    node.queue.addQueueEvent(transitionState,0,2,autoDoor,0);                
    autoCloseEventTrigger=1;
  }
  
  //Reset any timed events at 00:00
  if(hours==0 && minutes==0){
    resetEvents();
  }
  
  Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 
  inKey = customKeypad.getKey();
  handleInput(inKey);
  displayState();
}

//=========================================================================

void resetEvents(){
  autoOpenEventTrigger=0;
  autoCloseEventTrigger=0;
}

void resetStateTimes(){
  for(int i=0;i<4;++i){
    stateHours[i]=0;
    stateMinutes[i]=0;
  } 
}

//=========================================================================

int stateUpdate(char* message, int length){
  statusTimeout = millis(); //update the status timeout time
  node.handler.setMessage(message, length);
  int rStat[4];
  
  //node.handler.getNextMessageField();//get rid of field label for door1 status
  char* door1StateStr = node.handler.getNextMessageField();
  rStat[0] = atoi(door1StateStr);//door1state

  //node.handler.getNextMessageField();//get rid of field label for door2 status
  char* door2StateStr = node.handler.getNextMessageField();
  rStat[1]= atoi(door2StateStr);//door1error
  
  //node.handler.getNextMessageField();//get rid of field label for door2 status
  char* err1StateStr = node.handler.getNextMessageField();
  rStat[2] = atoi(err1StateStr);//door2state
  
  //node.handler.getNextMessageField();//get rid of field label for door2 status
  char* err2StateStr = node.handler.getNextMessageField();
  rStat[3] = atoi(err2StateStr);//door2error
  
  updateStates(rStat);

  //node.handler.getNextMessageField();//get rid of field label for door2 status
  char* ambLightStr = node.handler.getNextMessageField();
  ambLight = atoi(ambLightStr);
  
  //node.handler.getNextMessageField();//get rid of field label for door2 status
  char* lightStr = node.handler.getNextMessageField();
  lightState = atoi(lightStr);
  
  char* dayStateStr = node.handler.getNextMessageField();
  dayState = atoi(dayStateStr);

  return 1;
}

//=========================================================================

void updateStates(int* rStat){
  //for each state or error, check if it has changed
  //update the time for it
  for(int i = 0; i<4; ++i){
    if(rStat[i]==doorStates[i]){
      //do nothing
    }else{
      //update the state and set the time 
      doorStates[i]=rStat[i];
      stateHours[i]=hours;
      stateMinutes[i]=minutes;
    }
  }
}

//=========================================================================

int autoStateUpdate(char* message, int length){
  node.handler.setMessage(message, length);
  char* autoStr = node.handler.getNextMessageField();
  autoOpenThreshold = atoi(autoStr);

  autoStr = node.handler.getNextMessageField();
  autoOpenEnableType = atoi(autoStr);
  
  autoStr = node.handler.getNextMessageField();
  autoCloseThreshold = atoi(autoStr);

  autoStr = node.handler.getNextMessageField();
  autoCloseEnableType = atoi(autoStr);
  
  autoStr = node.handler.getNextMessageField();
  autoDoor = atoi(autoStr);
}

//=========================================================================

void transitionState(unsigned int num[]){
  int msgSize = sprintf(node.getBuffer(),"dor|num:%i;oc:%i",num[0],num[1]);
  node.sendToNode(msgSize,1);
}

//=========================================================================

void clearError(unsigned int num[]){
  int msgSize = sprintf(node.getBuffer(),"clr|num:%i",num[0]);
  node.sendToNode(msgSize,1);
}

//=========================================================================

void setLight(unsigned int onOff[]){
  int msgSize = sprintf(node.getBuffer(),"lght|sval:%i",onOff[0]);
  node.sendToNode(msgSize,1);
}

//=========================================================================

void requestStatus(unsigned int num[]){
  int msgSize = sprintf(node.getBuffer(),"stat|");
  
  node.sendToNode(msgSize,1);
}

//=========================================================================

void sendAutoParams(unsigned int params[]){
  int msgSize = sprintf(node.getBuffer(),"ato|%i;%i;%i",params[0],params[1],params[2]);
  
  node.sendToNode(msgSize,1);  
}

//=========================================================================

//MUST MUST MUST be called more often than 1000ms
void updateTime(){
   unsigned long currentTime = millis();
   unsigned long diff = currentTime-lastTime;
   if(diff > SECOND){
      seconds++;
      lastTime = currentTime - (diff%SECOND);
   }  
   if(seconds >=60){
     minutes++;
     seconds = 0;
   }  
   if(minutes >=60){
     hours++;
     minutes=0;
   }  
   if(hours >=24){
     hours = 0;
   }
}

//=========================================================================

void displayState(){
  display.clearDisplay();
  
  switch(menu){
    case 0:
      displayDefault();
      break;
    case 1:
      displayAutoSelect();
      break; 
    case 2:
      displaySettings();
      break;
  }
  display.display();
}

//=========================================================================

void displayDefault(){
    for(int i = 0; i<4;++i){
    int doorNum=i/2;//0 or 1
    if(i%2==0){//print door state
      int offset = doorNumberOffset * doorNum;
      displayFlashIcon(offset+doorStates[i], 0, 24*doorNum, 0, 0);
      printTimeDifference(stateHours[i],stateMinutes[i],0,16+24*doorNum);
    }else{//print error state
      displayFlashIcon(errorOffset+doorStates[i], 21, 24*doorNum, 0, 0);
      printTimeDifference(stateHours[i],stateMinutes[i],21,16+24*doorNum);
    }   
  }
  
  //is the coop light on or off?
  displayFlashIcon(lightStateOffset+lightState,42,24,0,0);
  
  //display the coop's understanding of night/day cycle and ambient light level
  displayFlashIcon(dayStateOffset+dayState,64,0,0,0);
  //displayFlashIcon(ambientLightOffset, 64, 0, 0, 0);
  display.setCursor(64,16);
  displayNum(ambLight,FONT_NORMAL);
  
  //display the time
  display.drawRoundRect(59, 24, 25, 15, 1, 1); 
  printTime(hours,minutes,63,28);

}

//=========================================================================

void displayAutoSelect(){
  /*display.println("  NO");
  display.println("  AO");
  display.println("  TO");
  
  display.println("  NC");
  display.println("  AC");
  display.println("  TC");
  
  display.setCursor(0,8*autoOpenEnableType);
  display.print(" *");
  
  display.setCursor(0,(8*autoCloseEnableType)+24);
  display.print(" *");
  
  display.setCursor(0,8*cursorLine);
  display.print(">"); 
  */
  for(int i=0;i<6;++i){
    char hShift=1; //the amount to shift the icons to make room for highlighting
    char column = i%3;
    char row = i/3;
    char highlight = 0;
    char selected = 0;
    if(cursorLine==i){
      highlight = 1;
    }
    
    if(row==0 && autoOpenEnableType==column){
      selected=1;
    }
    
    if(row==1 && autoCloseEnableType==column){
      selected=1;
    }
    
    displayFlashIcon(autoOpenOptionsOffset+i, //icon number
    hShift+(24*column), //x
    hShift+(24*row),    //y
    selected,           //selected
    highlight);         //highlighted
    
  }
  
}

//=========================================================================

void displaySettings(){

  displayFlashIcon(currentTimeOffset, 1, 1, cursorLine==0&&menuState==1, cursorLine==0);
  //display.print(" tm:");
  if(menuState==0){
    printTime(hours,minutes,0,18);
  }else{
    printTime(setTimeHours,setTimeMinutes,0,18);
  }
  
  displayFlashIcon(closeTimeOffset, 24, 1, cursorLine==1&&menuState==1, cursorLine==1);
  //display.print(" ct:");
  printTime(autoCloseTimeHours,autoCloseTimeMinutes,24,18);

  displayFlashIcon(openTimeOffset, 48, 1, cursorLine==2&&menuState==1, cursorLine==2);
  //display.print(" ot:");
  printTime(autoOpenTimeHours,autoOpenTimeMinutes,48,18);

  //second row
  displayFlashIcon(closeLightLevelOffset, 1, 25, cursorLine==3&&menuState==1, cursorLine==3);  
  //display.print(" cth:");
  display.setCursor(0,42);
  //display.println(autoCloseThreshold);
  displayNum(autoCloseThreshold, FONT_NORMAL);
  
  displayFlashIcon(openLightLevelOffset, 24, 25, cursorLine==4&&menuState==1, cursorLine==4);  
  //display.print(" oth:");
  display.setCursor(24,42);
  //display.println(autoOpenThreshold);
  displayNum(autoOpenThreshold, FONT_NORMAL);
  
  displayFlashIcon(autoDoorSelectOffset+autoDoor, 48, 25, 0, cursorLine==5);
  
  
  //display.print(" ad:");
  //display.println(autoDoor+1);//always add 1 for display
  
  //display.setCursor(0,8*cursorLine);
  /*if(menuState==0){
    display.print(">");
  }else{
    display.print("*");
  }*/
  
  //display.print(" ");
  //display.print(inKey);
  //display.setCursor(42,5*8);
  //display.print(freeMemory());
}

//=========================================================================

void printTimeDifference(char hrs, char mins, char x, char y){
  //convert provided time and system time to minutes
  //and subtract
  //display.setCursor(x,y);
  int sysMins = 60*hours + minutes;
  int printMins = 60*hrs+mins;
  int diffMins = sysMins-printMins;
  
  //convert back to hours/mins and print
  printTime(diffMins/60,diffMins%60,x,y);
}

//=========================================================================

void printTime(char hrs, char mins, char x, char y){
  display.setCursor(x,y);
  //if(hrs<10){
    //display.print("0");
    //displayNum(0);
  //}
  
  //display.print((int)hrs);
  displayNum((int)hrs, FONT_THIN);
  int xN=display.getCursorX();
  int yN=display.getCursorY();
  display.drawPixel(xN, yN+2, 1);
  display.drawPixel(xN, yN+4, 1);
  display.setCursor(xN+2,yN);
  if(mins<10){
    displayNum(0, FONT_THIN);
    //display.print("0"); 
  }//else{
    //display.print(":");
  //}
  //display.println((int)mins); 
  displayNum((int)mins, FONT_THIN); 
}

//=========================================================================

void handleInput(char input){
  if(input=='#' && menuState==0){
      //menuState=0;
      menu++;
      menu%=3;
      cursorLine=0; 
  }else if(input=='0' && menuState==0){
    //request update from the coop
    display.clearDisplay();
    display.print("R");
    display.display();
    delay(100);
    node.queue.addQueueEvent(requestStatus,0,0);
    node.queue.addQueueEvent(sendAutoParams,1000,3,4,0,0); 
  }else{
    switch(menu){
      case 0:
        handleDefaultInput(input);
      break;
      case 1:
        handleAutoSelectInput(input);
      break;
      case 2:
        if(menuState==0){
          handleSettingsSelect(input);
        }else{
          handleSettingsInput(input);
        }
      break;
    }
  }
}

//=========================================================================

void handleDefaultInput(char input){
    switch(input){
     case '1':
        //left door open
        node.queue.addQueueEvent(transitionState,0,2,0,1);  
     break; 
     case '4':
        //left door close
        node.queue.addQueueEvent(transitionState,0,2,0,0);  
     break; 
     case '7':
        //left door clear error
        node.queue.addQueueEvent(clearError,0,1,0);  
     break; 
     case '3':
        //right door open
        node.queue.addQueueEvent(transitionState,0,2,1,1);  
     break; 
     case '6':
        //right door close
        node.queue.addQueueEvent(transitionState,0,2,1,0);  
     break; 
     case '9':
        //right door clear error
        node.queue.addQueueEvent(clearError,0,1,1); 
     break; 
     case '2':
        //light on
        node.queue.addQueueEvent(setLight,0,1,1);  
     break; 
     case '5':
        //light off
        node.queue.addQueueEvent(setLight,0,1,0);  
     break;       
     default:
     break; 
    };
}

//=========================================================================

void handleAutoSelectInput(char input){
  switch(input){      
    case '1':
    case '3':
      cursorLine--;
      if(cursorLine<0){
        cursorLine=5;
      }
      break;
      
    case '4':
    case '6':
      cursorLine++;
      if(cursorLine>5){
        cursorLine=0;
      }
      break;
      
    case '*':
      selectAutoParameters();
    break;
    default:
    break;
  } 
}

//=========================================================================

void selectAutoParameters(){
  switch(cursorLine){
    case 0:
      autoOpenEnableType=AUTO_DISABLED;
      break;
    case 1:
      autoOpenEnableType=AUTO_ENABLED;
      break;
    case 2:
      autoOpenEnableType=AUTO_TIMED;
      break;
    case 3:
      autoCloseEnableType=AUTO_DISABLED;
      break;
    case 4:
      autoCloseEnableType=AUTO_ENABLED;
      break;
    case 5:
      autoCloseEnableType=AUTO_TIMED;
      break;
  }
  
  //update the coop with the selected enable value
  if(cursorLine<3){                         //delay,numArgs,autoVal,threshold,enable
    node.queue.addQueueEvent(sendAutoParams,0,3,0,autoOpenThreshold,autoOpenEnableType);
  }else{
    node.queue.addQueueEvent(sendAutoParams,0,3,1,autoCloseThreshold,autoCloseEnableType);
  }
} 

//=========================================================================

void handleSettingsSelect(char input){
    switch(input){
    case '*':
      //toggle menuState to edit the current selection
      if(cursorLine==5){
        saveSetting();
      }else{
        menuState=1; 
        setTimeHours=hours;
        setTimeMinutes=minutes;
        clearSetting();//clear the current selection
      }
      break;  
    
    //select the line
    case '1':
    case '3':
      cursorLine--;
      if(cursorLine<0){
        cursorLine=5;
      }
      break;
      
    case '4':
    case '6':
      cursorLine++;
      if(cursorLine>5){
        cursorLine=0;
      }
      break;      
      
      default:
      break;
    } 
}

//=========================================================================

void handleSettingsInput(char input){
  switch(input){ 
    case '#':
      //clear the current entry
      clearSetting();
    case '*':
      //toggle menuState to save the current selection
      saveSetting();//save the curr
      menuState=0;//return to item selection 
      break;  
      
    default:
      //change character to an integer number 
      //and update the selected setting
      int num=input-48;
      if(num>=0 && num <10){
        updateSetting(num);
      }
      break;
  }
}

//=========================================================================

void clearSetting(){
  switch(cursorLine){
    case 0:
      setTimeHours=0;
      setTimeMinutes=0;
    break;
    case 1:
      autoCloseTimeHours=0;
      autoCloseTimeMinutes=0;
    break;
    case 2:
      autoOpenTimeHours=0;
      autoOpenTimeMinutes=0;
    break;
    case 3:
      autoCloseThreshold=0;
    break;
    case 4:
      autoOpenThreshold=0;
    break;
    //case 5:
    //  autoDoor=0;
    //break;
  }
}

//=========================================================================

void updateSetting(int val){
    switch(cursorLine){
    case 0:
      setTimeVal(&setTimeHours,&setTimeMinutes, val);     
    break;
    case 1:
      setTimeVal(&autoCloseTimeHours,&autoCloseTimeMinutes, val);
    break;
    case 2:
      setTimeVal(&autoOpenTimeHours,&autoOpenTimeMinutes, val);
    break;
    case 3:
      autoCloseThreshold*=10;
      autoCloseThreshold+=val;
      if(autoCloseThreshold>1024){
        autoCloseThreshold=1024;
      }
    break;
    case 4:
      autoOpenThreshold*=10;
      autoOpenThreshold+=val;
      if(autoOpenThreshold>1024){
        autoOpenThreshold=1024;
      }
    break;
    /*case 5:
      autoDoor=val-1;//can only be 0 or 1
      if(autoDoor!=0&&autoDoor!=1){
        autoDoor=0;
      }
    break;*/
  }
}

//=========================================================================

 void setTimeVal(int* hrs,int* mins, int val){   
   int hrs10= *hrs%10;//*hrs/10;
   int hrs1= *mins/10;//*hrs%10;
   int mins10= *mins%10;//*mins/10;
   
   *hrs=10*hrs10 + hrs1;
   *mins= 10*mins10 + val;   
 }

//=========================================================================

void saveSetting(){
  //Save the following and send to the chicken coop (for threshold and door)
    switch(cursorLine){
    case 0:    
      hours = setTimeHours;
      minutes = setTimeMinutes;     
      resetEvents();
      resetStateTimes();
    break;
    case 1:    
      if(autoCloseTimeHours>23){
        autoCloseTimeHours=0;
      }
      if(autoCloseTimeMinutes>59){
        autoCloseTimeMinutes=0;
      }
      EEPROMIO.updateIndexData(autoCloseTimeHours,offsets[2]);
      EEPROMIO.updateIndexData(autoCloseTimeMinutes,offsets[3]);     
    break;
    case 2: 
      if(autoOpenTimeHours>23){
        autoOpenTimeHours=0;
      }
      if(autoOpenTimeMinutes>59){
        autoOpenTimeMinutes=0;
      }  
      EEPROMIO.updateIndexData(autoOpenTimeHours,offsets[0]);
      EEPROMIO.updateIndexData(autoOpenTimeMinutes,offsets[1]);
    break;
    case 3:    
      //autoCloseThreshold persisted in the coop not locally
      node.queue.addQueueEvent(sendAutoParams,0,3,1,autoCloseThreshold,autoCloseEnableType);
    break;
    
    case 4:
      //autoOpenThreshold persisted in the coop not locally
      node.queue.addQueueEvent(sendAutoParams,0,3,0,autoOpenThreshold,autoOpenEnableType);
    break;
    
    case 5:
      //autoDoor threshold persisted in the coop, not locally
      //autodoor 0->2, autodoor 1->3 , threshold and enable are not saved with this message
      autoDoor=1-autoDoor;
      node.queue.addQueueEvent(sendAutoParams,0,3,autoDoor+2,0,0);
    break;   
  }
  
}

//=========================================================================

void textBox(int x, int y, int width, int height){
  display.fillRect(x,y,width,height,WHITE);
  display.drawRect(x,y,width,height,BLACK);  
}

//=========================================================================

void displayFlashIcon(int iconNumber, int x, int y, char inverted, char highlight){
  unsigned char icon[32];
  flash.readBytes(32*iconNumber, icon,32);
  
  if(highlight==0){
    if(inverted==1){
      display.fillRoundRect(x, y, 15, 15, 1, 1);
      drawBitmap(x,y,icon,16,16,0);
    }else{
      display.drawRoundRect(x, y, 15, 15, 1, 1);
      drawBitmap(x,y,icon,16,16,1);
    }
  }else{
    if(inverted==1){
      display.fillRoundRect(x-1, y-1, 18, 18, 1, 1);
      drawBitmap(x,y,icon,16,16,0);
    }else{
      display.drawRoundRect(x-1, y-1, 18, 18, 1, 1);
      display.drawRect(x,y,16,16,1);
      drawBitmap(x,y,icon,16,16,1);
    }
  }
}

//=========================================================================

void drawBitmap(uint16_t x,uint16_t y, uint8_t *bitmap, int16_t w, int16_t h, uint16_t color){
  //for each bit set in the byte array, draw a pixel
  int bytesPerRow = w/8;
  if(w%8!=0){
    bytesPerRow++;
  }
  
  for(int i=0;i< h;++i){
    for(int j=0;j<w;++j){
      
      //get the byte we are looking for
      int rowStart=i*bytesPerRow;
      uint8_t byteVal = bitmap[rowStart+j/8];
      
      //get the bit we are looking for     
      int bitLoc = 7-(j%8);
      int bitVal= (byteVal >> bitLoc) & 0x01;
      if(bitVal!=0){
        display.drawPixel(x+j, y+i, color);
      }
    }
  }
}

void displayNum(int num,char fsize){
  if(num<10){
    displayNumChar(num,fsize);
    //display.setCursor(display.getCursorX()+5,display.getCursorY());
  }else {
    //recurse then display the least significant digit
    displayNum(num/10,fsize);
    displayNumChar(num%10,fsize);
    //display.setCursor(x+5,y);
  }
  if(fsize == FONT_THIN){
    display.setCursor(display.getCursorX()+4,display.getCursorY());
  }else{
    display.setCursor(display.getCursorX()+5,display.getCursorY());
  }
}

void displayNumChar(char num,char fsize){
  unsigned int x = display.getCursorX();
  unsigned int y = display.getCursorY();
  unsigned char letter[FONT_SIZE];
  if(fsize == FONT_THIN){
    flash.readBytes(SMALL_FONT_OFFSET+(num*FONT_SIZE), letter,FONT_SIZE);
    while(flash.busy());
    drawBitmap(x,y,letter,4,FONT_SIZE,1);
  }else{
  
    flash.readBytes(FONT_OFFSET+(num*FONT_SIZE), letter,FONT_SIZE);
    while(flash.busy());
    drawBitmap(x,y,letter,5,FONT_SIZE,1);
  }  
}
