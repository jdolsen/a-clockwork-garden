#ifndef A4988_ACCELERATION_H
#define A4988_ACCELERATION_H

#define MAX_NUM_PULSES 2500
#include "arduino.h"

#define NO_ENABLE 0
#define ENABLE_CAPABLE 1
#define AUTO_ENABLE 2

#define NUM_MOTORS 5
class A4988Acceleration{
  private:
  
  char stepPin[NUM_MOTORS];
  char directionPin[NUM_MOTORS];
  char enablePin[NUM_MOTORS];
  char doEnableDisable[NUM_MOTORS];
  
  char* notificationFlag[NUM_MOTORS]; //pointer to the flag polled by the main thread
                                      //Note that this reduces accuracy, but it maintains
                                      //simplicity for the ISR function
  int notificationSteps[NUM_MOTORS]; //if non-zero, decrement every step.  when zero, set the flag
  int recurringNotificationSteps[NUM_MOTORS]; //saves notification steps for recurring notifications
  
  volatile char decelerating[NUM_MOTORS];
  long acceleration[NUM_MOTORS];
  long instAcceleration[NUM_MOTORS];
  long stepsToDecelerate[NUM_MOTORS];
  volatile long accelPulseCount[NUM_MOTORS];
  int ticksPerVelocityChange[NUM_MOTORS];
  
  long velocity[NUM_MOTORS];
  volatile long instVelocity[NUM_MOTORS];
  long maxVelocity[NUM_MOTORS];
  char count[NUM_MOTORS]; //a raw count from the timer
  volatile int pulseNum[NUM_MOTORS];
  int ticksPerPulse[NUM_MOTORS];
  volatile int stepCount[NUM_MOTORS];// the number of steps executed
  volatile char finishedFlag[NUM_MOTORS];
  
  volatile char isContinuous[NUM_MOTORS];
  void determineMovementProfile(int steps, char motor);
  long RPMFactor;
  
  void  autoEnableMotor(char motor);
  void  autoDisableMotor(char motor);

  
  public:
  A4988Acceleration();
  void initInt();
  void initMotor(char sPin, char dPin, char ePin, char motor);
  void initMotor(char sPin, char dPin, char motor);
  void doISR();
  
  void setAcceleration(long stepsPerSecond,char motor);
  char setVelocity(long stepsPerSecond,char motor);
  char setMaxVelocity(long stepsPerSecond,char motor);
  void doSteps(int steps,char motor);
  int waitForSteps(int steps,char motor);
  int waitForMotorFinished(char motor);
  
  //Notification functions
 
  void notifyAfterSteps(int steps, char& flag, char isRecurring, char motor);
  void endNotify(char motor);
  
  //Continuous Mode functions
  void startContinuous(char direction, char motor);
  int decelerateContinuous(char motor);//returns steps remaining
  int stopContinuous(char motor); //returns steps remaining

  void  enableMotor(char motor);
  void  disableMotor(char motor);
  void  setEnableDisable(char isAuto, char motor);
  
  //RPM functions
  void initRPM(int stepsPerRotation, int microstepsPerStep);
  void setRPMAcceleration(double RPM2,char motor);
  char setRPMVelocity(double RPM,char motor);
  char setRPMMaxVelocity(double RPM,char motor);
  void doRotations(double RPM,char motor);
  int waitForRotations(double RPM,char motor);
  
};
extern A4988Acceleration Driver;
#endif