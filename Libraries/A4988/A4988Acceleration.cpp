#include "A4988Acceleration.h"
#include "Arduino.h"


//---------------------------------------------------------------------------------------

 A4988Acceleration::A4988Acceleration(){
	
	for(int i=0;i<NUM_MOTORS;++i){
		velocity[i]=0;
		maxVelocity[i]=MAX_NUM_PULSES;

		count[i]=0;
		pulseNum[i]=0;
		ticksPerPulse[i]=0;
		stepCount[i]=0;
		finishedFlag[i]=0;
		instAcceleration[i]=0;
		isContinuous[i]=0;
		
		notificationFlag[i]=0;
		notificationSteps[i]=0;
	}
		
}

//---------------------------------------------------------------------------------------

ISR(TIMER1_COMPA_vect){//timer2 interrupt 1kHz toggles pin 9 at 2Hz
	Driver.doISR();
}

void A4988Acceleration::initInt(){
		//interrupt every 200uscs (5KHz) (2.5KHz max pulse rate) this is the maximum pulse width for these steppers
	cli();
	TCCR1A = 0;// set entire TCCR1A register to 0
	TCCR1B = 0;// same for TCCR1B
	TCNT1  = 0;//initialize counter value to 0
	// set compare match register for 5kHz increments
	OCR1A = 399;// = (16*10^6) / (5000*64) - 1 (must be <65536 for timer 1)	  
	TCCR1B |= (1 << WGM12); // turn on CTC mode	  
	TCCR1B |= (1 << CS11); // Set CS11 bit for 8 prescaler  
	TIMSK1 |= (1 << OCIE1A);// enable timer compare interrupt
	sei();
}

void A4988Acceleration::initMotor(char sPin, char dPin, char ePin, char motor){
	pinMode(sPin, OUTPUT);
	pinMode(dPin, OUTPUT);
	pinMode(ePin, OUTPUT);
	
	stepPin[motor]=sPin;
	directionPin[motor]=dPin;
	enablePin[motor]=ePin;
	
	digitalWrite(directionPin[motor],0);
	digitalWrite(stepPin[motor],0);	 
	digitalWrite(enablePin[motor],1);
	doEnableDisable[motor]=AUTO_ENABLE;
	autoDisableMotor(motor);
 }
 
 
 void A4988Acceleration::initMotor(char sPin, char dPin, char motor){
	pinMode(sPin, OUTPUT);
	pinMode(dPin, OUTPUT);
	
	stepPin[motor]=sPin;
	directionPin[motor]=dPin;
	
	digitalWrite(directionPin[motor],0);
	digitalWrite(stepPin[motor],0);	 
	doEnableDisable[motor]=NO_ENABLE;
	
 }

//---------------------------------------------------------------------------------------

void A4988Acceleration::doISR(){
	//every other tick, increment the pulses and toggle the step pulse line if ticks per pulse has been reached
	//update steps per second, velocity, and acceleration	

	for(int i=0;i<NUM_MOTORS;++i){	
		count[i]++;
		if(count[i]%2==1){	
			//update velocity based on acceleration value
			if(acceleration[i]!=0){
				accelPulseCount[i]++;
				
				if(accelPulseCount[i]>=ticksPerVelocityChange[i]){
					accelPulseCount[i]=0;
					if(decelerating[i]==0){					
						instVelocity[i]+=instAcceleration[i];//instVelocity = sqrt(2*acceleration*currentStep);
					}else{
						instVelocity[i]-=instAcceleration[i];//instVelocity = sqrt(2*acceleration*stepCount);
					}
								
					if(instVelocity[i]>=maxVelocity[i]){
						instVelocity[i]=maxVelocity[i];
					}
					
					if(decelerating[i]==0){//do not recalculate velocity here during deceleration (do when a pulse is sent)
						if(instVelocity[i]!=0){
							ticksPerPulse[i]=MAX_NUM_PULSES/instVelocity[i];
						}else{
							ticksPerPulse[i]=0;//don't move if the velocity is zero
						}
						
					}
				}
			}//end acceleration handling
			
			//determine if pulse should be sent based on velocity
			pulseNum[i]++;//pulseNum must never be more than MAX_NUM_PULSES
			if(pulseNum[i]>=ticksPerPulse[i] && ticksPerPulse[i]!=0 && (stepCount[i]>0 || isContinuous[i]==1)){
				//this is for the benefit of the decelerating phase when ticks per pulse is not calculated above
				if(instVelocity[i]!=0){
					ticksPerPulse[i]=MAX_NUM_PULSES/instVelocity[i];
				}else{
					ticksPerPulse[i]=0;//don't move if the velocity is zero
				}
				
				digitalWrite(stepPin[i],1);
				if(isContinuous[i]==0){
					stepCount[i]--;
				}else{
					stepCount[i]++;
				}
				
				//determine if notifications should be triggered
				if(notificationSteps[i]!=0){
					notificationSteps[i]-=1;
					if(notificationSteps[i]<=0){
						//notify
						char* tempFlag = notificationFlag[i];
						*tempFlag=1;
						
						//if notification is recurring, reset notificationSteps
						if(recurringNotificationSteps[i]!=0){
							notificationSteps[i]=recurringNotificationSteps[i];
						}
					}		
				}
				
				pulseNum[i]=0;
			}	

			//determine if deceleration	should begin (don't auto-decelerate in continuous mode (isContinuous==1))
			if(stepCount[i] <= stepsToDecelerate[i] && stepCount[i]!=0 && decelerating[i]==0 && isContinuous[i]==0){			
				decelerating[i]=1;
			}
		
			//determine if end conditions are met
			if((stepCount[i]<=0 &&isContinuous[i]==0)||(instVelocity[i]<=0 && decelerating[i]==1 && isContinuous[i]==1)){ //Ensure velocity 
													//will NEVER reach 0.  If velocity ever reached 0 before steps were 
													//complete this would not return. 
													//added instVelocity term to conditional for decelerate from continuous mode
													
				instVelocity[i] = velocity[i];      //when the steps reaches zero, reset the velocity
				pulseNum[i]=0;
				finishedFlag[i]=1;		
			}
		
		}else{
			count[i]=0;
			//set step pulse line to 0
			digitalWrite(stepPin[i],0);
		}
	}
}

//---------------------------------------------------------------------------------------

char A4988Acceleration::setVelocity(long stepsPerSecond,char motor){
		
	velocity[motor] = stepsPerSecond;	
	char ret=0;
	if(velocity[motor]<0){
		velocity[motor]=-velocity[motor];		
	}
	
	if(velocity[motor]>maxVelocity[motor]){
		velocity[motor]=maxVelocity[motor];
		ret=1;
	}

	if(velocity[motor]!=0){
		ticksPerPulse[motor]=maxVelocity[motor]/velocity[motor];
	}else{
		ticksPerPulse[motor]=0;//don't move if the velocity is zero
	}	
	
	instVelocity[motor]=velocity[motor];
	return ret;
}

//---------------------------------------------------------------------------------------

char A4988Acceleration::setMaxVelocity(long stepsPerSecond,char motor){
	maxVelocity[motor] = stepsPerSecond;
	char ret=0;
	//negative velocities are not allowed
	if(maxVelocity[motor]<0){
		maxVelocity[motor]=-maxVelocity[motor];
	}
	
	if(velocity[motor]>MAX_NUM_PULSES){
		velocity[motor]=MAX_NUM_PULSES;
		ret=1;
	}
	
	//ensure the velocity is not greater than MAX_NUM_PULSES
	if(maxVelocity[motor] > MAX_NUM_PULSES){
		maxVelocity[motor] = MAX_NUM_PULSES;
	}
	return ret;
}

//---------------------------------------------------------------------------------------

void A4988Acceleration::setAcceleration(long stepsPerSecond, char motor){
	decelerating[motor] = 0;

	acceleration[motor] = stepsPerSecond;
	if(acceleration[motor]<0){
		acceleration[motor]=-acceleration[motor];
	}
	long incrPerPulse = acceleration[motor]/MAX_NUM_PULSES;
	if(incrPerPulse!=0){
		ticksPerVelocityChange[motor]=1;
		instAcceleration[motor]=incrPerPulse;
	}else{
		if(acceleration[motor]!=0){
			ticksPerVelocityChange[motor] = MAX_NUM_PULSES/acceleration[motor];
			instAcceleration[motor]=1;					
		}
	}
}

//---------------------------------------------------------------------------------------

void A4988Acceleration::doSteps(int steps,char motor){
	autoEnableMotor(motor);
	determineMovementProfile(steps, motor);
	decelerating[motor]=0;
	
	if(steps>0){
		digitalWrite(directionPin[motor],1);
		stepCount[motor]=steps;
	}else{
		digitalWrite(directionPin[motor],0);
		stepCount[motor]=-steps;
	}
	//velocity = 1; //set velocity to a small value so the interrupt doesn't prematurely end the steps
	
}

//---------------------------------------------------------------------------------------

//returns any steps remaining
int A4988Acceleration::waitForSteps(int steps,char motor){
	autoEnableMotor(motor);
	decelerating[motor]=0;
	
	if(steps>0){
		digitalWrite(directionPin[motor],1);
		stepCount[motor]=steps;
	}else{
		digitalWrite(directionPin[motor],0);
		stepCount[motor]=-steps;
	}
	//Serial.println(stepCount);
	int stemp=stepCount[motor];
	determineMovementProfile(stemp,motor);
	finishedFlag[motor]=0;
	while(finishedFlag[motor]==0);
	autoDisableMotor(motor);
	return stepCount[motor];
}
 
//---------------------------------------------------------------------------------------

void A4988Acceleration::determineMovementProfile(int steps,char motor){
	//determines the number of steps it takes to decelerate
	int pulses = 1;
	int stepsDone = 0;
	int prevStepPulse = 0;
	long currentVelocity=0;
	long pulsesUntilStep=0;

	while(1){ 		
		currentVelocity = (pulses * acceleration[motor])/MAX_NUM_PULSES;
		pulsesUntilStep = prevStepPulse+ (MAX_NUM_PULSES/currentVelocity);
		if(pulses>=pulsesUntilStep){
			prevStepPulse=pulses;
			stepsDone++;
		}
	
		if(stepsDone >=steps/2||currentVelocity>=maxVelocity[motor]){
			stepsToDecelerate[motor] = stepsDone;
			break;
		}
		pulses++;
	}
}

//---------------------------------------------------------------------------------------

int  A4988Acceleration::waitForMotorFinished(char motor){
	while(finishedFlag[motor]==0);
	autoDisableMotor(motor);
	return stepCount[motor];
}

//---------------------------------------------------------------------------------------

void A4988Acceleration::startContinuous(char direction, char motor){
	digitalWrite(directionPin[motor],direction);
	autoEnableMotor(motor);
	decelerating[motor] = 0;
	stepCount[motor]=0;
	isContinuous[motor]=1;
	finishedFlag[motor]=0;
}

//---------------------------------------------------------------------------------------

int A4988Acceleration::decelerateContinuous(char motor){
	
	if(finishedFlag[motor]==1){

	}else{
		//begin and wait for motor deceleration
		
		decelerating[motor]=1;
		finishedFlag[motor]=0;
		
		//wait for deceleration to finish and return the total step count
		while(finishedFlag[motor]==0);
		
	}
	//stop the motor
	pulseNum[motor]=0;
	isContinuous[motor]=0;
	//save the number of steps
	int steps = stepCount[motor];
	
	//set number of steps to zero and the finished flag to 1
	stepCount[motor]=0;
	finishedFlag[motor]=1;
	autoDisableMotor(motor);
	//return total step count
	return steps;
}

//---------------------------------------------------------------------------------------

int A4988Acceleration::stopContinuous(char motor){
	//stop the motor1
	pulseNum[motor]=0;
	isContinuous[motor]=0;
	
	//save the number of steps
	int steps = stepCount[motor];
	
	//set number of steps to zero and the finished flag to 1
	stepCount[motor]=0;
	finishedFlag[motor]=1;
	autoDisableMotor(motor);
	//return total step count
	return steps;
}

//---------------------------------------------------------------------------------------

void A4988Acceleration::initRPM(int stepsPerRotation, int microstepsPerStep){
	RPMFactor = stepsPerRotation*microstepsPerStep;
}

//---------------------------------------------------------------------------------------

void  A4988Acceleration::setRPMAcceleration(double RPM2,char motor){
	long steps = RPM2*RPMFactor;
	setAcceleration(steps,motor);
}

//---------------------------------------------------------------------------------------

char  A4988Acceleration::setRPMVelocity(double RPM,char motor){
	long steps = RPM*RPMFactor;
	return setVelocity(steps,motor);
}

//---------------------------------------------------------------------------------------

char  A4988Acceleration::setRPMMaxVelocity(double RPM,char motor){
	long steps = RPM*RPMFactor;
	return setMaxVelocity(steps,motor);
}

//---------------------------------------------------------------------------------------

void  A4988Acceleration::doRotations(double RPM,char motor){
	long steps = RPM*RPMFactor;
	doSteps(steps,motor);
}

//---------------------------------------------------------------------------------------

int  A4988Acceleration::waitForRotations(double RPM,char motor){
	long steps = RPM*RPMFactor;
	return waitForSteps(steps,motor);
}

//---------------------------------------------------------------------------------------

void A4988Acceleration::notifyAfterSteps(int steps, char& flag, char isRecurring, char motor){
	notificationSteps[motor]=steps;
	notificationFlag[motor]=flag;
	if(isRecurring!=0){
		recurringNotificationSteps[motor]=steps;
	}else{
		recurringNotificationSteps[motor]=0;
	}
		
}

//---------------------------------------------------------------------------------------

void A4988Acceleration::endNotify(char motor){
	notificationSteps[motor]=0;
	recurringNotificationSteps[motor]=0;
}

//---------------------------------------------------------------------------------------

//private methods
void  A4988Acceleration::autoEnableMotor(char motor){
	if(doEnableDisable[motor]==NO_ENABLE || doEnableDisable[motor]==ENABLE_CAPABLE){
		//do nothing since autoenabled is not set
	}else{
		digitalWrite(enablePin[motor],0);
	}
}

void  A4988Acceleration::autoDisableMotor(char motor){
	if(doEnableDisable[motor]==NO_ENABLE || doEnableDisable[motor]==ENABLE_CAPABLE){
		//do nothing since autoenabled is not set
	}else{
		digitalWrite(enablePin[motor],1);
	}
}

//public methods
void  A4988Acceleration::enableMotor(char motor){
	if(doEnableDisable[motor]==NO_ENABLE){
		//do nothing
	}else{
		digitalWrite(enablePin[motor],0);
	}
}

void  A4988Acceleration::disableMotor(char motor){
	if(doEnableDisable[motor]==NO_ENABLE){
		//do nothing
	}else{
		digitalWrite(enablePin[motor],1);
	}
}

 void A4988Acceleration::setEnableDisable(char autoVal, char motor){
	if(doEnableDisable[motor]==NO_ENABLE || autoVal==NO_ENABLE){
		//do not change to or from the NO_ENABLE state
	}else{
	  doEnableDisable[motor]=autoVal;
	}
 }
  
  

A4988Acceleration Driver = A4988Acceleration();