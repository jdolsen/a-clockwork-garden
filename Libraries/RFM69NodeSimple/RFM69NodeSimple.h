#ifndef RFM69NodeV2_H
#define RFM69NodeV2_H

#include <RFM69.h>
#include <EventQueue.h>
#include <CommandHandler.h>
//#include <EEPROMIO.h>


//#define NETWORKID    99  //the network ID we are on
//#define GATEWAYID   127  //the node ID we're sending to
#define ACK_TIMEOUT  150  // # of ms to wait for an ack before retrying the message
//#define MESSAGE_RETRIES 3 //not to exceed 127!!!
//#define NODE_CONNECTING_TIMEOUT 60000 //# of ms to wait while trying to establish a 
										//connection to the gateway

//#define NODE_INIT 0
//#define NODE_START 1
//#define NODE_CONNECT 2
//#define NODE_CONNECTING 3
//#define NODE_CONNECT_WAIT 4
//#define NODE_RUNNING 5
//#define NODE_DISCONNECTING 6
//#define NODE_DISCONNECTED 7
//#define NODE_PRINT
//#define TIMING_PRINT
//#define MSSG_PRINT

//#define MESSAGE_FAILURES_BEFORE_DISCONNECT 3
//#define MAX_TIME_BEFORE_COMM_RX 120000 //2 minute Gateway alive timer

const unsigned long NODE_TICK = 100L; //msec
const unsigned long NODE_TICK_US = 10000L;//100000L;
const unsigned long NODE_TIMER_US = 10L;
//#define CONNECTING_WINDOW_DURATION 900 //msecs
//#define CONNECTING_WINDOW_WAIT 3000
const int QSIZE=4;
const int MESSAGE_SIZE=130;

//const int NAMESIZE=12+1;//add 1 for the terminating character
	 
class RFM69NodeSimple{
	public:
	static CommandHandler handler;
	static EventQueue queue;
		
	private:
	QueueElement eventQueue[QSIZE];
	bool queueEnabled;	
	static RFM69 radio;
	static char payload[MESSAGE_SIZE];
	static char* nodeKey;
	//static long connectElapsedTime;	

	
	//========================================================================================================
	public:	
		static byte nodeid;
	static unsigned int netId;
	char* getBuffer();
	void nodeInit(char* key, unsigned int netId,unsigned int nodeId, uint8_t band);
	unsigned long nodeLoop(unsigned long delayMs=10000);
	boolean sendToNode(int size,  int node, int ackReq=3);
	
	//========================================================================================================

	private:
	
	//Message Receivers
	void receiveWirelessCommand();
};

extern RFM69NodeSimple node;


#endif