
#include <RFM69NodeSimple.h>



//STATIC INITIALIZATION
	CommandHandler nodeHandler;
	EventQueue nodeQueue;
	RFM69 nodeRadio;
	
	char RFM69NodeSimple::payload[MESSAGE_SIZE] = {0};
	char* RFM69NodeSimple::nodeKey="A";
	CommandHandler RFM69NodeSimple::handler=nodeHandler;
	EventQueue RFM69NodeSimple::queue=nodeQueue;
	RFM69 RFM69NodeSimple::radio=nodeRadio;
	//long RFM69NodeSimple::connectElapsedTime=0;	
	byte RFM69NodeSimple::nodeid = 126;
	//byte RFM69NodeSimple::nodeState = NODE_INIT;	
	//unsigned int RFM69NodeSimple::randTime=0;
	//unsigned int RFM69NodeSimple::nodeSN=0;
	//unsigned int RFM69NodeSimple::badCrcCount=0;
	unsigned int RFM69NodeSimple::netId=0;
	//int RFM69NodeSimple::connectState = 1;
	//unsigned long RFM69NodeSimple::timeSinceLastCommRx = 0;
	
	volatile unsigned long nodeTimerUs = 0UL;//holds the time for the full node run
	volatile unsigned long nodeTickTimerUs = 0UL; //holds the time for a single node tick
	volatile unsigned long nodeACKTimer = 0UL; //holds the time while waiting for an ack response

	
	
	//char nodeName[NAMESIZE+1];
	//int delayTime;
//========================================================================================================
//Class methods
//========================================================================================================	

	char* RFM69NodeSimple::getBuffer(){
		return payload;
	}
	
//========================================================================================================
	
	void RFM69NodeSimple::nodeInit(char* key, unsigned int nId, unsigned int nodeId, uint8_t band){

	  nodeKey = key;
	  netId = nId;
	  nodeid = nodeId;
	  //timeSinceLastCommRx =0;
	  queueEnabled=true;

	  radio.initialize(band, nodeid, netId);
	  radio.encrypt(nodeKey);
	  queue.initEvents(eventQueue, QSIZE, &queueEnabled);//, NODE_TICK);
	  queue.clear();
	  
	  //set timer2 interrupt at 100kHz (10us)
		  cli();
		  TCCR2A = 0;// set entire TCCR1A register to 0
		  TCCR2B = 0;// same for TCCR1B
		  TCNT2  = 0;//initialize counter value to 0
		  // set compare match register for 100kHz increments
		  OCR2A = 19;// = (16*10^6) / (100000*8) - 1 (must be <256)	  
		  TCCR2A |= (1 << WGM21); // turn on CTC mode	  
		  TCCR2B |= (1 << CS21); // Set CS21 bit for 8 prescaler  
		  TIMSK2 |= (1 << OCIE2A);// enable timer compare interrupt
		  sei();

	  
	  #ifdef NODE_PRINT
	  //Serial.println("Initialized");
	  #endif
	}
	
//========================================================================================================
//  Utility Functions
//========================================================================================================

ISR(TIMER2_COMPA_vect){//timer2 interrupt 1kHz toggles pin 9 at 2Hz
	nodeTimerUs+=NODE_TIMER_US; //gets reset on every node loop
	nodeTickTimerUs+=NODE_TIMER_US; //gets reset on every node tick 
									//(there may be several ticks in every run)
	nodeACKTimer+=NODE_TIMER_US;
}	
	
//========================================================================================================
//delayMs is the amount of time the nodeLoop function can run until it must return
unsigned long RFM69NodeSimple::nodeLoop(unsigned long dMs){
	unsigned long delayUs = 1000UL * dMs;
	
	//reset the timer for this run
	nodeTimerUs=0UL;
	//run until a break is signaled
	while(1){
		
		//timer was reset at start of last tick
		//wait until timer counts to the node_tick_time before ticking again
		//will tick immediately if the time has already expired			
		//while(nodeTickTimerUs < NODE_TICK_US);
		//while(nodeTickTimerUs < NODE_TICK_US);
		while(nodeTickTimerUs < NODE_TICK_US){asm("");}//this is done 3 times since the comparison often allows early exit

		unsigned long elapsedTimeUs = nodeTickTimerUs; //time spent outside the node function
													   //this is used to ensure a tick of the node clock 
													   //does not occur more often than NODE_TICK_US (see below)
			
		nodeTickTimerUs=0UL;  //reset the timer at the beginning of this node tick
		//timeSinceLastCommRx+=elapsedTimeUs/1000UL;
		//run node functions
		receiveWirelessCommand();
		queue.executeFirstValidEvent(elapsedTimeUs/1000UL);
		
		long remainingTimeUs = delayUs-nodeTimerUs;
		
		//node functions complete.  determine if another tick should be executed.
		//if remaining time is less than node_tick_time, wait for remaining
		//time to expire and exit	
		if(remainingTimeUs < NODE_TICK_US || remainingTimeUs < 0){
			//wait the remaining time and break
			//while(nodeTimerUs < delayUs);
			//while(nodeTimerUs < delayUs);
			while(nodeTimerUs < delayUs){asm("");}//this is done 3 times since the while comparison exits early sometimes
			break;
		}else{
			//run again
		}
	  }//end while
	  return nodeTimerUs;
}

//========================================================================================================

	boolean RFM69NodeSimple::sendToNode(int size,  int node, int numRetries){	

		//Before sending to the gateway, always check to see if a message was 
		  //received (a call to receiveWirelessCommand receives, parses and handles  
		  //the message and queues any message senders) This is necessary if there 
		  //was an ack timeout or a long time spent out of the node loop.
		payload[size+2]='\0';

		//if requesting retries this time...
		if(numRetries>0){		   
			//try sending the message until max retries is reached or the send is successful
			boolean messageSuccess = true;
			//int retryCount = 0;
		    for(int retryCount = 0; retryCount < numRetries; ++retryCount){
				
				//receiveWirelessCommand(); 
				radio.send(node, payload, size, true);	
				messageSuccess = true;
				
				//waits up to ACK_TIMEOUT seconds for an ack
				nodeACKTimer = 0;
				while(1){
					//if ack is received, break with success
					if(radio.ACKReceived(node)){
						break;
					}else if(nodeACKTimer >= (ACK_TIMEOUT*1000UL)){ 
						//if time expires, set message success false and break to retry the message again
						messageSuccess = false;
						break;
					}		
				}//end ack wait loop
				
				if(messageSuccess){//stop retrying the message if successful
					break;
				}
			}//end retry for loop
	
			return messageSuccess;			
			
		}else{//no retries requested
			receiveWirelessCommand();
			radio.send(node, payload, size);
			return true;
		}	
		  
		//FFFV: determine connection health - based on message failures and act accordingly
		//FFFV:  (feature for future version) Adjust transmit power levels 
		    //(0 (max) through 7 (min)) periodically set the power to one value 
			//lower and initiate a ping exchange similarly, if power level is not 
			//max and a disconnect is detected (or there are a lot of bad crcs) 
			//adjust power to see if aconnection can be re-established
	}
	

	
	
//========================================================================================================
//  Message Receivers
//========================================================================================================

void RFM69NodeSimple::receiveWirelessCommand(){
   if (radio.receiveDone()){
	  
		  handler.handleCommand(radio.DATA, radio.DATALEN);
		  if (radio.ACKRequested()){
              radio.sendACK(); 
          } 

  }
}

//========================================================================================================

RFM69NodeSimple node;
