#ifndef __COOP_DOOR_STEPPER_H
#define __COOP_DOOR_STEPPER_H


#include <A4988Acceleration.h>
//door states
#define INIT_STATE 0

//lock states
#define LOCKED_STATE 1
#define LOCKED_OPENING_STATE 2 //door sensor may still register locked while opening

//transition states
#define UNLOCKED_STATE 3 //generic state used to indicate the door is in transition
#define DOOR_OPENING_STATE 4
#define DOOR_CLOSING_STATE 5
#define DOOR_CLOSING_STOPPED_STATE 6
#define DOOR_OPENING_STOPPED_STATE 7

//open states
#define OPEN_STATE 8
#define OPEN_CLOSING_STATE 9 //door sensor may still register open while closing
#define NO_CHANGE_STATE 10

//errors
#define OPEN_ERROR 0
#define CLOSE_ERROR 1
#define OPENING_TIMEOUT_ERROR 2
#define CLOSING_TIMEOUT_ERROR 3
#define OPEN_INPUT_ERROR 4
#define CLOSE_INPUT_ERROR 5
#define MOTOR_OVERLOAD_ERROR 6
#define MOTOR_UNDERLOAD_ERROR 7
#define NO_ERROR 8

class CoopDoorStepper{

	private:
	char motorNum;
	char buttonPin;
	char openPin;
	char lockPin;
	char state;
	char errorState;
	char doorOpen;
	char doorLock;
	char homeStateErrorFlag;
	bool userInput;
	char buttonReleased;	
	unsigned long closingTime; //120 max seconds to close the door
	unsigned long openingTime; //180 max seconds to open the door
	unsigned long transitionTime; //10 max seconds to clear the 
                                      //door sensors and transition out of open or locked state
	unsigned long startTime;	
	//char obstructionFlag;
	//char underLoadedFlag;
	
	public:
	CoopDoorStepper(char mpin1, char mpin2, char ePin, char motorNum, int buttonPin, int doorOpenPin, int doorLockPin);

	char manage();
	char getState();
	char getError();
	void openDoor();
	void closeDoor();
	void clearError();
	char isClosed();
	char isOpened();
	char isTransiting();
	
	private:
	void getDoorSensorState();
	void manageStates();
	void doStateActions();
	bool didUserPressButton();
	void motorOff();
	void motorOpen();
	void motorClose();
};

#endif