
#include "Arduino.h"
#include "CoopDoorStepper.h"

CoopDoorStepper::CoopDoorStepper(char sPin, char dPin, char ePin, char motor, int button, int doorOpen, int doorLock)
  :motorNum(motor),
  buttonPin(button),
  openPin(doorOpen),
  lockPin(doorLock)
{	
	Driver.initMotor(sPin,dPin,ePin,motorNum);
	Driver.setAcceleration(500,motorNum);
	Driver.setMaxVelocity(600,motorNum);
	pinMode(openPin, INPUT);
    pinMode(lockPin, INPUT);
    pinMode(button,INPUT);
	state = INIT_STATE;
	errorState = NO_ERROR;
	userInput = false;
	buttonReleased = true;
	closingTime = 120000; //120 max seconds to close the door
	openingTime = 180000; //180 max seconds to open the door
	transitionTime = 10000; //10 max seconds to clear the 
                                      //door sensors and transition out of open or locked state
	startTime = 0;	
	errorState = NO_ERROR;
	homeStateErrorFlag=0;
	//obstructionFlag = 0;
	//underLoadedFlag = 0;
}

//----------------------------------------------------------------------------------

char CoopDoorStepper::manage(){
    getDoorSensorState(); 
    userInput = didUserPressButton(); 
    manageStates();   
    doStateActions();//do the actions for the current state
	return state;	
}

//----------------------------------------------------------------------------------

char CoopDoorStepper::getState(){
	return state;
}

//----------------------------------------------------------------------------------

char CoopDoorStepper::getError(){
	return errorState;
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::getDoorSensorState(){
  //check sensors to figure out door state
  doorOpen = digitalRead(openPin);
  doorLock = digitalRead(lockPin);
 
  //If currently moving
  /*if(state == LOCKED_OPENING_STATE ||
     state == DOOR_OPENING_STATE ||
     state == DOOR_CLOSING_STATE ||
     state == OPEN_CLOSING_STATE){
       //int motorVal = analogRead(MOTOR_SENSE_PIN);
       //check for an obstruction (motor is overloaded
       //if(motorVal > OBSTRUCTION_THRESHOLD){
       //  obstructionFlag = 1;
       //}else{
       //  obstructionFlag=0;
       //}
       
       //check for hardware disconnect (motor is underloaded)
       //if(((state == LOCKED_OPENING_STATE || state == DOOR_OPENING_STATE)&& (motorVal < OPENING_LOAD_THRESHOLD)) ||
       //   ((state == DOOR_CLOSING_STATE   || state == OPEN_CLOSING_STATE)&& (motorVal < CLOSING_LOAD_THRESHOLD))){
       //      underLoadedFlag = 1;
       // }else{
       //  underLoadedFlag = 0;
       //}     
       
   }else{//not moving
     obstructionFlag=0;
     underLoadedFlag = 0;
   }
   */
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::manageStates(){ 
  //change this big switch statement to two calls:
  //char newState = getNextState(); //uses a LUT with sensor input and state to figure out the next state
  //char newError = getErrorCode(); //does same to figure out if there was an error
  
  //determine new state based on sensors and user input
  switch(state){
    case INIT_STATE:
      if(doorOpen){
        state = OPEN_STATE;
      }else if(doorLock){
        state = LOCKED_STATE;
      }else{
        state = DOOR_CLOSING_STOPPED_STATE;       
      }
      //user input doesn't matter in init state
    break;
    
    case LOCKED_STATE:
      if(doorOpen){
        //ERROR: TAMPERING!!!
		errorState = CLOSE_INPUT_ERROR;
		homeStateErrorFlag = 1;
      }else if(doorLock && !doorOpen){
        //Do nothing
		//clear any errors from previous states
		//will not clear errors that occurred in this state
		if(homeStateErrorFlag==0){
			errorState=NO_ERROR;
		}
      }else{
        //ERROR: TAMPERING!!! 
		errorState = CLOSE_INPUT_ERROR;
		homeStateErrorFlag = 1;		
      }
        
      if(userInput){
        state = LOCKED_OPENING_STATE;
        startTime = millis();
		homeStateErrorFlag = 0;//cleared when transitioning out of a home state
      }
    break;
    
    case LOCKED_OPENING_STATE:
       if(doorOpen){
        //Transition to see if there is an error
        state = OPEN_STATE;
      }else if(doorLock){
        //If timeout is detected, ERROR, transition to locked state, 
        unsigned long currentTime = millis();
        if(currentTime-startTime > transitionTime){
          //ERROR
		  errorState = OPENING_TIMEOUT_ERROR;
          state= LOCKED_STATE;
        } 
      }else{
        state = DOOR_OPENING_STATE; 
        startTime = millis();      
      }
        
      if(userInput){
        state = DOOR_OPENING_STOPPED_STATE;
      }
    break;
    
    case DOOR_OPENING_STATE: 
      if(doorOpen){
        state = OPEN_STATE;
      }else if(doorLock){
        //ERROR: mechanical
		errorState = OPEN_INPUT_ERROR;
        state = LOCKED_STATE;
      }else{
        unsigned long currentTime = millis();
        if(currentTime-startTime > openingTime){
          //ERROR
		  errorState = OPENING_TIMEOUT_ERROR;
          state= DOOR_OPENING_STOPPED_STATE; //door is stuck in the middle?
                                             //door lock detection switch is broken?
                                             //door open detection switch is broken?
                                             //tampering?
        }        
      }
        
      if(userInput){
        state = DOOR_OPENING_STOPPED_STATE;
      }
    break;
    
    case DOOR_CLOSING_STATE: 
      if(doorOpen){
        //ERROR: tampering?
		errorState = CLOSE_INPUT_ERROR;
        state = OPEN_STATE;
      }else if(doorLock){       
        state = LOCKED_STATE;
      }else{
        unsigned long currentTime = millis();
        if(currentTime-startTime > closingTime){
          //ERROR
		  errorState = CLOSING_TIMEOUT_ERROR;
          state= DOOR_CLOSING_STOPPED_STATE; //door is stuck in the middle?
                                             //door lock detection switch is broken?
                                             //door open detection switch is broken?
                                             //tampering?
        }             
      }
        
      if(userInput){
        state = DOOR_CLOSING_STOPPED_STATE;
      }
    break;
    
    case DOOR_CLOSING_STOPPED_STATE:
      if(doorOpen){
        state = OPEN_STATE;//if the user stopped the door while it was still open
      }
    
      if(userInput){
        if(doorOpen){
          state = OPEN_CLOSING_STATE;//start closing the door again since it is already open
        }else if(doorLock){
          state = LOCKED_OPENING_STATE;
        }else{
          state = DOOR_OPENING_STATE;      
        }
        
        startTime = millis();      
      } 
    break;
    
    case DOOR_OPENING_STOPPED_STATE: 
      if(doorLock){
        state = LOCKED_STATE; //If the user stopped the door while it was still locked  
      }
    
      if(userInput){
        if(doorOpen){
          state = OPEN_CLOSING_STATE;
        }else if(doorLock){
          state = LOCKED_OPENING_STATE; //start opening the sdoor again since it is already locked
        }else{
          state = DOOR_CLOSING_STATE;       
        }
        startTime = millis();      
      } 
    break;
    
    case OPEN_STATE:
      if(doorOpen&!doorLock){
        //Do nothing
		//clear any errors from previous states
		//will not clear errors that occurred in this state
		if(homeStateErrorFlag==0){
			errorState=NO_ERROR;
		}
      }else if(doorLock){
        //Error mechanical
		errorState = OPEN_INPUT_ERROR;		
		//set an error flag that will persist
		homeStateErrorFlag = 1;
      }else{
        //Error mechanical 
		errorState=OPEN_INPUT_ERROR;	
		//set an error flag that will persist
		homeStateErrorFlag = 1;
      }
        
      if(userInput){
        state = OPEN_CLOSING_STATE;
        startTime = millis();
		//clear this flag since no longer in a home state
		homeStateErrorFlag = 0;
      }
    break;

    case OPEN_CLOSING_STATE:
       if(doorOpen){          
        //If timeout is detected, ERROR, transition to open state,
        unsigned long currentTime = millis();
        if(currentTime-startTime > transitionTime){
          //ERROR
		  errorState = CLOSING_TIMEOUT_ERROR;
          state= DOOR_CLOSING_STOPPED_STATE; //door lock detection switch is broken?
                                             //door open detection switch is broken?
                                             //tampering?
        }        
      }else if(doorLock){
        //Goto locked state to see if an error can be detected.
        state = LOCKED_STATE;
      }else{
        state = DOOR_CLOSING_STATE;
        startTime = millis();     
      }
        
      if(userInput){
        state = DOOR_CLOSING_STOPPED_STATE;
      }
    break; 
  }
  //determine actions based on previous state new state, and flags
  
  //if the door is closing and the lock sensors are triggered, signal door closed and stop the motor
  //if the door is opening and the fully open sensor is triggered, signal door opened and stop the motor
  
  //if the door state changed without user input, triple check then signal an error 
    //(example, the door cable broke and the door is no longer open or in transit when it should be)
    //(example, the door was forced open (it is no longer locked when it should be))
  
  //1 if the motor is overloaded, signal error 
  //2 if the door motion is opening and the motor is underloaded, signal an error
  //3 if the door motion is opening and the locks do not disengage at an appropriate time, signal an error
  //4 if the door motion is closing and the locks do not engage at an appropriate time, signal an error
  //5 if the door motion is opening and the full open sensor does not engage at an appropriate time, signal an error  
  //6 if the door motion is closing and the full open sensor does not disengage at an appropriate time, signal an error
  //FOR ALL NEWLY DETECTED ERRORS (error state change), STOP THE MOTOR
  //ENSURE THE ORDERING OF THE ABOVE ERROR DETECTION SCHEMES IS SUCH THAT ERRORS ARE LESS LIKELY TO MASK ONE ANOTHER
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::doStateActions(){

  switch(state){
    case LOCKED_STATE:    
      motorOff();
    break;
    case LOCKED_OPENING_STATE:
      motorOpen();       
      //update timeout timer and check to see if there is a timeout condition
    break;
    case DOOR_OPENING_STATE: 
      motorOpen();
    break;
    case DOOR_CLOSING_STATE: 
      motorClose();
    break;
    case OPEN_CLOSING_STATE: 
      motorClose();
      //update timeout timer and check to see if there is a timeout condition
    break;
    case DOOR_CLOSING_STOPPED_STATE:
      motorOff(); 
    break;
    case DOOR_OPENING_STOPPED_STATE: 
      motorOff();
    break;
    case OPEN_STATE: 
      motorOff();
    break;
  }
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::openDoor(){

  switch(state){
    case LOCKED_STATE:    
		state = LOCKED_OPENING_STATE;
		homeStateErrorFlag = 0; //clear this flag since no longer in a home state
    break;
    case LOCKED_OPENING_STATE:
		//do nothing state = LOCKED_STATE;
    break;
    case DOOR_OPENING_STATE: 
		//do nothing state = DOOR_CLOSING_STATE;
    break;
    case DOOR_CLOSING_STATE: 
		state = DOOR_OPENING_STATE;		
    break;
    case OPEN_CLOSING_STATE: 
		state = OPEN_STATE;
    break;
    case DOOR_CLOSING_STOPPED_STATE:
		state = DOOR_OPENING_STATE;
    break;
    case DOOR_OPENING_STOPPED_STATE: 
		state = DOOR_OPENING_STATE;
    break;
    case OPEN_STATE: 
		//do nothingstate = OPEN_CLOSING_STATE;
    break;
  }
  startTime = millis();
}

void CoopDoorStepper::closeDoor(){

  switch(state){
    case LOCKED_STATE:    
		//do nothing
    break;
    case LOCKED_OPENING_STATE:
		state = LOCKED_STATE;
    break;
    case DOOR_OPENING_STATE: 
		state = DOOR_CLOSING_STATE;
    break;
    case DOOR_CLOSING_STATE: 
		//Do nothing		
    break;
    case OPEN_CLOSING_STATE: 
		//do nothing
    break;
    case DOOR_CLOSING_STOPPED_STATE:
		state = DOOR_CLOSING_STATE;
    break;
    case DOOR_OPENING_STOPPED_STATE: 
		state = DOOR_CLOSING_STATE;
    break;
    case OPEN_STATE: 
		state = OPEN_CLOSING_STATE;
		homeStateErrorFlag = 0; //clear this flag since no longer in a home state
    break;
  }
  startTime = millis();
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::clearError(){
	errorState = NO_ERROR;
}

//----------------------------------------------------------------------------------

char CoopDoorStepper::isClosed(){
	return state==LOCKED_STATE;
	
}

//----------------------------------------------------------------------------------

char CoopDoorStepper::isOpened(){
	return state==OPEN_STATE;	
}

//----------------------------------------------------------------------------------

char CoopDoorStepper::isTransiting(){
	return (state!=LOCKED_STATE)&&(state!=OPEN_STATE);	
}

//----------------------------------------------------------------------------------

bool CoopDoorStepper::didUserPressButton(){
    int buttonState = digitalRead(buttonPin);
    
    //ONLY return true if the button was previously released and has now 
    //been pressed (1).
    if(buttonState == 1 && buttonReleased == true){
      buttonReleased = false;
      return true;
    }else if(buttonState == 0){//change button released to true if button is currently not pressed (0)
      buttonReleased = true;
    }
    
    //if the button was not pressed OR the buttonReleased was false
    return false;
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::motorOff(){
  //do nothing
  //motor.motorOff();
  Driver.stopContinuous(motorNum);
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::motorOpen(){
  //small_stepper.step(2048);//cw 1 revolution
  Driver.startContinuous(0, motorNum);
}

//----------------------------------------------------------------------------------

void CoopDoorStepper::motorClose(){
  //small_stepper.step(-2048);//ccw 1 revolution
  Driver.startContinuous(1, motorNum);
}
//----------------------------------------------------------------------------------


