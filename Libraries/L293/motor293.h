
#ifndef __MOTOR293_H
#define __MOTOR293_H

class Motor293{
	
	private:

	
	public:
		Motor293(int m1pin1, int m1pin2);
		
		void motorForward();
		void motorBackward();
		void motorOff();
	private:

		int pin1;
		int pin2;
};

#endif