#include "Arduino.h"
#include "motor293.h"

	Motor293::Motor293(int m1pin1, int m1pin2){
		pin1 = m1pin1;
		pin2 = m1pin2;		
		pinMode(pin1,OUTPUT);
		pinMode(pin2,OUTPUT);
	}
	
	void Motor293::motorForward(){
		motorOff();
		digitalWrite(pin1,0);
		digitalWrite(pin2,1);
	}
	
	void Motor293::motorBackward(){
		motorOff();
		digitalWrite(pin1,1);
		digitalWrite(pin2,0);
	}
	
	void Motor293::motorOff(){
		digitalWrite(pin1,0);
		digitalWrite(pin2,0);
	}