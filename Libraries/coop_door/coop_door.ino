#include <Arduino.h>
//#include <RFM69registers.h>
#include <SPI.h>
#include <RFM69.h>
#include <EEPROMIO.h>
#include <CommandHandler.h>
#include <EventQueue.h>
#include <RFM69NodeSimple.h>
#include <pgmspace.h>

#include <A4988Acceleration.h>
#include <CoopDoorStepper.h>

//both doors use the same enable pin since neither motor should run at the same time
CoopDoorStepper doors[2] = {
  CoopDoorStepper(3,4,8,0,14,15,16),
  CoopDoorStepper(5,6,9,1,17,18,19)
};

#define DAY_INIT 0
#define NIGHT 1
#define WAIT_OPEN 2
#define DAY 4
#define WAIT_CLOSE 5

#define AUTO_DISABLED 0
#define AUTO_ENABLED 1
#define AUTO_TIMED 2

#define WAIT_TIME 60000*10 //currently defined as 10 minutes

#define DISPLAY_CLOSED 0
#define DISPLAY_TRANSITION 1
#define DISPLAY_OPEN 2
#define DISPLAY_ERROR 3

int lightPin = 7;
char lightState = 0;
//int indicatorLightPin[2] = {8,9};
#define INDICATOR_LIGHT_PIN 1
#define AMBIENT_PIN A6
int ambLight;

typedef struct{
  int threshold;
  int enabled;
  
} autoParam;

unsigned int offsets[3];
char autoDoor;
autoParam autoOpen;
autoParam autoClose;

int dayState = DAY_INIT;

char lastState[2] = {0,0};
char lastError[2] = {NO_ERROR,NO_ERROR};

int loops = 0;
unsigned long time;
unsigned long dayLoops=0;

//===================================================================================

void setup(){
  UCSR0B = 0;
  //doors[0]=CoopDoorStepper(3,4,0,0,14,15,16);
  
  //doors[1]=CoopDoorStepper(5,6,1,1,17,18,19);
  offsets[0] = EEPROMIO.loadInit(autoOpen);
  offsets[1] = EEPROMIO.loadInit(autoClose); 
  offsets[2] = EEPROMIO.loadInit(autoDoor); 
  
  /*
  autoOpen.threshold=200;
  //autoOpen.time=15;
  autoOpen.enabled=1;
  
  autoClose.threshold=200;
  //autoClose.time=15;
  autoClose.enabled=0;
   
  autoDoor=0;
  
  EEPROMIO.updateIndexData(autoOpen, offsets[0]);
  EEPROMIO.updateIndexData(autoClose,offsets[1]);
  EEPROMIO.updateIndexData(autoDoor, offsets[2]);
  */
  
  node.nodeInit("COOPABCDCOOPABCD", 10, 1, RF69_915MHZ);
  //Serial.println("started");  
  pinMode(lightPin,OUTPUT);
  //pinMode(indicatorLightPin[0],OUTPUT);
  //pinMode(indicatorLightPin[1],OUTPUT);
  
  node.handler.registerCommandHandler(
  "dor", changeDoor, "num,oc");
  
  node.handler.registerCommandHandler(
  "clr", clearError, "num");
  
  node.handler.registerCommandHandler(
  "lght", setLight, "sval");
  
  node.handler.registerCommandHandler(
  "ato", handleAuto, "oc,th,t,e");
  
  //for simple nodes, it is not necessary to register data senders
  //since there is no connection to the host. Therefore, this could 
  //registered as a command handler.
  node.handler.registerDataSender(
  "stat", 
    handleStateRequest, 
    "");
    
  Driver.initInt();
  node.queue.addQueueEvent(sendState,0,0);
  node.queue.addQueueEvent(sendAutoParams,0,0);
  Serial.end();
  pinMode(INDICATOR_LIGHT_PIN,OUTPUT);
  manageDayState();
}

//===================================================================================

void loop(){
   manageDoors(); 
   
   //Only manage the day state every minute or so
   if(dayLoops>=50000){
     manageDayState();
     dayLoops=0;
   }else{
     dayLoops+=300;    
   }

   displayState();
   node.nodeLoop(300);
   
}

//===================================================================================

void manageDayState(){
  //int previousAmbLight = ambLight;
  ambLight = getLight();
  switch(dayState){
    case DAY_INIT:
      if(ambLight<=autoOpen.threshold){//chose this since initial threshold is likely to be zero                                      
        dayState=NIGHT;
      }else{
        dayState=DAY;
      }
      node.queue.addQueueEvent(sendState,0,0);
      break;
      
    case NIGHT:
      //Wait for light threshold to go above the threshold (indicating daybreak)
      if(ambLight>=autoOpen.threshold){
        time=millis();
        dayState=WAIT_OPEN;
        node.queue.addQueueEvent(sendState,0,0);
      }
      break;
      
    case WAIT_OPEN:      
      //if ambLight goes under the threshold, **return to the NIGHT state**
      if(ambLight<autoOpen.threshold){
        dayState=NIGHT; 
        node.queue.addQueueEvent(sendState,0,0);
      }else if(millis()-time>=WAIT_TIME){ //wait some amount of time before opening
        dayState=DAY;
        if(autoOpen.enabled==AUTO_ENABLED){//only open if autoOpen is enabled
          doors[autoDoor].openDoor();
        }
        node.queue.addQueueEvent(sendState,0,0);
      }
      break;
      
    case DAY:
      //wait for the light value to go under the threshold (indicating night fall) 
      if(ambLight<=autoClose.threshold){
        time=millis();
        dayState=WAIT_CLOSE; 
        node.queue.addQueueEvent(sendState,0,0);
      }
      break;
      
    case WAIT_CLOSE:      
      //if ambLight goes above the threshold, **return to the DAY state**
      if(ambLight>autoClose.threshold){
        dayState=DAY; 
        node.queue.addQueueEvent(sendState,0,0);
      }else if(millis()-time>=WAIT_TIME){//wait some amount of time before closing
        dayState=NIGHT;
        if(autoClose.enabled==AUTO_ENABLED){//only close if autoClose is enabled
          doors[autoDoor].closeDoor();
        }
        node.queue.addQueueEvent(sendState,0,0);
      }
      break;
  }
}

//===================================================================================

void manageDoors(){
   int state1 = doors[0].manage();
   int err1 = doors[0].getError();
   
   int state2 = doors[1].manage();    
   int err2 = doors[1].getError();
   
   if(state1!=lastState[0]||
      err1 != lastError[0]||
      state2!=lastState[1]||
      err2 != lastError[1]){
     //report the new state and update lastState
     node.queue.addQueueEvent(sendState,0,0);
     lastState[0] = state1;
     lastError[0] = err1;
     lastState[1] = state2;
     lastError[1] = err2;
   }   
}

//===================================================================================

int changeDoor(char* message, int length){
  node.handler.setMessage(message, length);
  node.handler.getNextMessageField();//get rid of field label
  char* doorNumStr = node.handler.getNextMessageField();
  int num = atoi(doorNumStr);
 
  node.handler.getNextMessageField();//get rid of field label
  char* openCloseStr = node.handler.getNextMessageField();
  int oc = atoi(openCloseStr); 
  
  //transition the door based on the door's current state
  if(oc==1){
    doors[num].openDoor();
    //Serial.println("opening");
  }else{
    doors[num].closeDoor();
    //Serial.println("closing");
  }
  return 1;
}

//===================================================================================

int clearError(char* message, int length){
  node.handler.setMessage(message, length);
  node.handler.getNextMessageField();//get rid of field label
  char* doorNumStr = node.handler.getNextMessageField();
  int num = atoi(doorNumStr);
  doors[num].clearError();
  node.queue.addQueueEvent(sendState,0,0);
  return 1;
}

//===================================================================================

int setLight(char* message, int length){
  node.handler.setMessage(message, length);
  node.handler.getNextMessageField();//get rid of field label
  char* lightValStr = node.handler.getNextMessageField();
  int lightVal = atoi(lightValStr);
  //Set the light
  if(lightVal>0){
    digitalWrite(lightPin,1);
    lightState=1;
  }else{
    digitalWrite(lightPin,0);
    lightState=0;
  }
  node.queue.addQueueEvent(sendState,0,0);
  return 1;
}

//===================================================================================

int handleAuto(char* message, int length){
  node.handler.setMessage(message, length);
  
  //auto-close or auto-open or auto door value or parameter request
  //node.handler.getNextMessageField(); //get rid of field label
  char* autoValStr = node.handler.getNextMessageField();
  int autoVal = atoi(autoValStr);
  
  //threshold1
  //node.handler.getNextMessageField(); //get rid of field label
  char* t1ValStr = node.handler.getNextMessageField();
  int t1 = atoi(t1ValStr);
  
  //enable
  //node.handler.getNextMessageField(); //get rid of field label
  char* enableValStr = node.handler.getNextMessageField();
  char enable = atoi(enableValStr);
  
  //updateAutoPresets(autoVal,t1,t2,time,enable); 
  if(autoVal==0){//set autoOpen params
    autoOpen.threshold=t1;
    autoOpen.enabled=enable;
    EEPROMIO.updateIndexData(autoOpen,offsets[0]);
    
  }else if(autoVal==1){//set autoClose Params
    autoClose.threshold=t1;
    autoClose.enabled=enable;
    EEPROMIO.updateIndexData(autoClose,offsets[1]);
    
  }else if(autoVal==2){//set auto door to 0
    autoDoor=0;
    EEPROMIO.updateIndexData(autoDoor,offsets[2]);
    
  }else if(autoVal==3){//set auto door to 1
    autoDoor=1;
    EEPROMIO.updateIndexData(autoDoor,offsets[2]);
    
  }else if(autoVal==4){//send auto door parameters
    node.queue.addQueueEvent(sendAutoParams,0,0);
  }
  
  return 1; 
}

//===================================================================================

int handleStateRequest(char* message, int length){
  node.queue.addQueueEvent(sendState,0,0);
  return 1;
}

//===================================================================================

void sendState(unsigned int noValues[]){
  //ambLight = getLight();
  manageDayState();
  //int msgSize = sprintf(node.getBuffer(),"stat|st1:%i;er1:%i;st2:%i;er2:%i;amb:%i;lt:%i;st:%i",
  int msgSize = sprintf(node.getBuffer(),"stat|%i;%i;%i;%i;%i;%i;%i",
    doors[0].getState(),doors[0].getError(),doors[1].getState(),doors[1].getError(),ambLight,lightState,dayState);
  node.sendToNode(msgSize,2);
  //TODO: if send to node is unsucessful, queue message again for 10 seconds from now
  //node.queue.addQueueEvent(sendState,10000,0);
}

//===================================================================================

void sendAutoParams(unsigned int noValues[]){
  
  int msgSize = sprintf(node.getBuffer(),"atp|%i;%i;%i;%i;%i",
    autoOpen.threshold, autoOpen.enabled,
    autoClose.threshold, autoClose.enabled, autoDoor);
  node.sendToNode(msgSize,2);
  //TODO: if send to node is unsucessful, queue message again for 10 seconds from now
  //node.queue.addQueueEvent(sendState,10000,0);
}

//===================================================================================

void displayState(){
  loops++;
  char displayVal=DISPLAY_CLOSED;
  
  for(int i = 0; i<2; ++i){
    if(doors[i].getError()!=NO_ERROR){
      changeDisplay(&displayVal,DISPLAY_ERROR);      
    }else if(lastState[i] == OPEN_STATE){
      changeDisplay(&displayVal,DISPLAY_OPEN);
    }else if(lastState[i] == LOCKED_STATE){
      changeDisplay(&displayVal,DISPLAY_CLOSED);
    }else{
      changeDisplay(&displayVal,DISPLAY_TRANSITION);
    }
  }
  
  switch(displayVal){
    case DISPLAY_CLOSED:
      digitalWrite(INDICATOR_LIGHT_PIN,0);//turn off the light
      break;
      
    case DISPLAY_ERROR:
       //toggle the pin every loop
       digitalWrite(INDICATOR_LIGHT_PIN, !digitalRead(INDICATOR_LIGHT_PIN));
      break;
      
    case DISPLAY_TRANSITION:
      //toggle the pin every 3rd loop
      if(loops%3==0){
          digitalWrite(INDICATOR_LIGHT_PIN, !digitalRead(INDICATOR_LIGHT_PIN));
      }
      break;
      
    case DISPLAY_OPEN:
      digitalWrite(INDICATOR_LIGHT_PIN,1);//turn on the light
      break;
  }
}

//===================================================================================

void changeDisplay(char* displayVal, char newVal){
    if(*displayVal < newVal){
      *displayVal=newVal;
    }  
}

//===================================================================================

double getLight(){
  int numLoops = 10;
  double total = 0;
  for(int i = 0; i<numLoops; ++i){
    total+=analogRead(AMBIENT_PIN);
    delay(2);//delay 1ms 
  }
  return total/numLoops;
}

