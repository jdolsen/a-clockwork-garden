#include <Arduino.h>
#include <EEPROMIO.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

#include <SPIFlash.h>    //get it here: https://github.com/LowPowerLab/SPIFlash
#include <SPI.h>

#ifdef __AVR_ATmega1284P__
  #define LED           15 // Moteino MEGAs have LEDs on D15
  #define FLASH_SS      23 // and FLASH SS on D23
#else
  #define LED           9 // Moteinos have LEDs on D9
  #define FLASH_SS      8 // and FLASH SS on D8
#endif

// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out (SCLK)
// pin 6 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(19,18,17,16);

//////////////////////////////////////////
// flash(SPI_CS, MANUFACTURER_ID)
// SPI_CS          - CS pin attached to SPI flash chip (8 in case of Moteino)
// MANUFACTURER_ID - OPTIONAL, 0x1F44 for adesto(ex atmel) 4mbit flash
//                             0xEF30 for windbond 4mbit flash
//////////////////////////////////////////
SPIFlash flash(FLASH_SS, 0xEF30);

int autoCloseTimeHours=0;
int autoCloseTimeMinutes=0;
int autoOpenTimeHours=0;
int autoOpenTimeMinutes=0;

typedef struct iconStorage{
  uint32_t adx;
  int size;
} IconStorage;

//#define NUM_ICONS 200
#define NUM_TO_SAVE 54
#define START_AT_INDEX 0
#define ICON_SIZE 32
//IconStorage icon;

static const unsigned char icons[NUM_TO_SAVE][ICON_SIZE] PROGMEM={
  //door1 init 0
  {0, 0, 0, 0, 15, 240, 8, 16, 9, 144, 10, 80, 8, 80, 12, 80, 8, 144, 9, 16, 9, 16, 8, 16, 9, 16, 15, 240, 0, 0, 0, 0},
  //door1 closed 1
  {0, 0, 0, 0, 63, 128, 32, 128, 46, 128, 42, 128, 46, 128, 36, 128, 36, 136, 44, 152, 36, 136, 44, 136, 32, 136, 63, 156, 0, 0, 0, 0},
  //door1 open started 2
  {0, 0, 0, 0, 63, 192, 32, 64, 32, 64, 32, 64, 32, 64, 48, 64, 38, 72, 47, 88, 32, 72, 47, 72, 32, 72, 63, 220, 0, 0, 0, 0},
  //door 1 holder (//state 3 not used) 3
  {127, 254, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 127, 254},
  //door1 opening (same as open started)  4
  {0, 0, 0, 0, 63, 192, 32, 64, 32, 64, 32, 64, 32, 64, 48, 64, 38, 72, 47, 88, 32, 72, 47, 72, 32, 72, 63, 220, 0, 0, 0, 0},
  //door1 closing 5
  {0, 0, 0, 0, 63, 192, 32, 64, 32, 64, 32, 64, 32, 64, 32, 64, 47, 72, 32, 88, 47, 72, 38, 72, 32, 72, 63, 220, 0, 0, 0, 0},
  //door 1 closing stopped 6
  {0, 0, 0, 0, 63, 168, 32, 168, 32, 168, 32, 168, 32, 128, 32, 128, 46, 136, 32, 152, 46, 136, 36, 136, 32, 136, 63, 156, 0, 0, 0, 0},
  //door 1 opening stopped 7
  {0, 0, 0, 0, 63, 168, 32, 168, 32, 168, 32, 168, 32, 128, 48, 128, 36, 136, 46, 152, 32, 136, 46, 136, 32, 136, 63, 156, 0, 0, 0, 0},
  //door 1 open 8
  {0, 0, 0, 0, 63, 128, 36, 128, 36, 128, 36, 128, 38, 128, 36, 128, 36, 136, 36, 152, 36, 136, 34, 136, 33, 136, 32, 156, 0, 0, 0, 0},
  //door 1 closing started (same as closing) 9
  {0, 0, 0, 0, 63, 192, 32, 64, 32, 64, 32, 64, 32, 64, 32, 64, 47, 72, 32, 88, 47, 72, 38, 72, 32, 72, 63, 220, 0, 0, 0, 0},
  
  //door 2 init 10
  {0, 0, 0, 0, 15, 240, 8, 16, 9, 144, 10, 80, 8, 80, 12, 80, 8, 144, 9, 16, 9, 16, 8, 16, 9, 16, 15, 240, 0, 0, 0, 0},
  //door 2 closed 11
  {0, 0, 0, 0, 63, 128, 32, 128, 46, 128, 42, 128, 46, 128, 52, 128, 36, 152, 44, 164, 36, 132, 44, 152, 32, 160, 63, 188, 0, 0, 0, 0},
  //door2 open started 12
  {0, 0, 0, 0, 63, 128, 32, 128, 32, 128, 32, 128, 32, 128, 48, 128, 36, 152, 46, 164, 32, 132, 46, 152, 32, 160, 63, 188, 0, 0, 0, 0},
  //door2 holder (//state 3 not used) 13
  {127, 254, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 128, 1, 127, 254},
  //door2 opening (same as open started) 14
  {0, 0, 0, 0, 63, 128, 32, 128, 32, 128, 32, 128, 32, 128, 48, 128, 36, 152, 46, 164, 32, 132, 46, 152, 32, 160, 63, 188, 0, 0, 0, 0},
  //door 2 closing 15
  {0, 0, 0, 0, 63, 128, 32, 128, 32, 128, 32, 128, 32, 128, 48, 128, 46, 152, 32, 164, 46, 132, 36, 152, 32, 160, 63, 188, 0, 0, 0, 0},
  //door2 closing stopped 16
  {0, 0, 0, 0, 63, 168, 32, 168, 32, 168, 32, 168, 32, 128, 48, 128, 46, 152, 32, 164, 46, 132, 36, 152, 32, 160, 63, 188, 0, 0, 0, 0},
  //door 2 opening stopped 17
  {0, 0, 0, 0, 63, 168, 32, 168, 32, 168, 32, 168, 32, 128, 48, 128, 36, 152, 46, 164, 32, 132, 46, 152, 32, 160, 63, 188, 0, 0, 0, 0},
  //door 2 open 18
  {0, 0, 0, 0, 63, 128, 40, 128, 40, 128, 40, 128, 44, 128, 40, 128, 40, 152, 40, 164, 40, 132, 44, 152, 35, 160, 32, 188, 0, 0, 0, 0},
  //door 2 closing started (same as closing) 19
  {0, 0, 0, 0, 63, 128, 32, 128, 32, 128, 32, 128, 32, 128, 48, 128, 46, 152, 32, 164, 46, 132, 36, 152, 32, 160, 63, 188, 0, 0, 0, 0},
  
  
  //Daystate init (question mark) 20
  {0, 0, 0, 0, 7, 128, 15, 192, 28, 224, 24, 96, 0, 96, 0, 224, 1, 192, 3, 128, 3, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 0},
  //daystate night 21
  {0, 0, 0, 0, 2, 0, 14, 32, 18, 112, 18, 32, 34, 8, 34, 28, 33, 8, 32, 128, 16, 120, 16, 8, 12, 48, 3, 192, 0, 0, 0, 0},
  //daystate night wait 22
  {0, 0, 0, 0, 3, 192, 12, 128, 17, 0, 17, 224, 35, 88, 34, 72, 36, 68, 36, 124, 20, 4, 18, 8, 15, 24, 3, 224, 0, 0, 0, 0},
  //daystate unknown (warning)23
  {0, 0, 0, 0, 1, 128, 3, 192, 2, 64, 6, 96, 5, 160, 5, 160, 13, 176, 9, 144, 24, 24, 17, 136, 48, 12, 63, 252, 0, 0, 0, 0},
  //daystate day 24
  {0, 0, 4, 32, 32, 4, 19, 200, 4, 32, 8, 16, 80, 10, 16, 8, 16, 8, 80, 10, 8, 16, 4, 32, 19, 200, 32, 4, 2, 64, 0, 0},
  //daystate day wait 25
  {0, 0, 4, 32, 32, 4, 19, 200, 4, 32, 8, 16, 80, 250, 17, 72, 18, 68, 82, 116, 10, 68, 6, 4, 19, 8, 32, 244, 2, 0, 0, 0},
  
  
  //open error 26
  {0, 0, 0, 0, 63, 128, 40, 152, 40, 152, 40, 152, 44, 152, 40, 152, 40, 152, 40, 152, 36, 128, 34, 152, 33, 152, 32, 128, 0, 0, 0, 0},
  //close error 27
  {0, 0, 0, 0, 63, 128, 32, 152, 46, 152, 42, 152, 46, 152, 36, 152, 36, 152, 44, 152, 36, 128, 44, 152, 32, 152, 63, 128, 0, 0, 0, 0},
  //open timeout 28
  {0, 0, 0, 0, 63, 240, 32, 248, 33, 60, 33, 60, 33, 4, 32, 136, 32, 112, 32, 72, 38, 72, 47, 72, 32, 64, 63, 200, 0, 0, 0, 0},
  //close timeout 29
  {0, 0, 0, 0, 63, 240, 32, 248, 33, 60, 33, 60, 33, 4, 32, 136, 32, 112, 32, 72, 47, 72, 38, 72, 32, 64, 63, 200, 0, 0, 0, 0},
  //open sensor error 30
  {0, 0, 0, 0, 63, 192, 32, 88, 38, 88, 41, 88, 32, 88, 38, 88, 41, 88, 32, 88, 38, 64, 47, 88, 32, 88, 63, 192, 0, 0, 0, 0},
  //close sensor error 31
  {0, 0, 0, 0, 63, 192, 32, 88, 38, 88, 41, 88, 32, 88, 38, 88, 41, 88, 32, 88, 47, 64, 38, 88, 32, 88, 63, 192, 0, 0, 0, 0},
  //motor overcurrent (not used) 32
  {0, 0, 0, 0, 31, 224, 32, 32, 45, 188, 42, 188, 40, 160, 31, 224, 0, 0, 0, 192, 8, 192, 28, 192, 8, 0, 0, 192, 0, 0, 0, 0},
  //motor undercurrent (not used) 33
  {0, 0, 0, 0, 31, 224, 32, 32, 45, 188, 42, 188, 40, 160, 31, 224, 0, 0, 0, 192, 0, 192, 28, 192, 0, 0, 0, 192, 0, 0, 0, 0},
  //no error 34
  {0, 0, 0, 0, 0, 0, 0, 8, 0, 28, 16, 60, 56, 124, 56, 252, 61, 252, 63, 252, 31, 240, 15, 192, 7, 0, 0, 0, 0, 0, 0, 0},
  
  
  //warning 35
  {0, 0, 0, 0, 1, 128, 3, 192, 2, 64, 6, 96, 5, 160, 5, 160, 13, 176, 9, 144, 24, 24, 17, 136, 48, 12, 63, 252, 0, 0, 0, 0},
  //X 36
  {0, 0, 0, 0, 16, 8, 56, 28, 28, 56, 14, 112, 7, 224, 3, 192, 3, 192, 7, 224, 14, 112, 28, 56, 56, 28, 16, 8, 0, 0, 0, 0},
  
  //NO (/) 37
  {0, 0, 0, 0, 3, 192, 15, 240, 28, 56, 24, 120, 48, 236, 49, 204, 51, 140, 55, 12, 30, 24, 28, 56, 15, 240, 3, 192, 0, 0, 0, 0},
  
  //light on 38
  {0, 0, 0, 0, 0, 0, 3, 192, 7, 224, 15, 176, 15, 208, 15, 208, 15, 176, 7, 224, 3, 192, 3, 192, 2, 64, 1, 128, 0, 0, 0, 0},

  //light off 39
  {0, 0, 0, 0, 16, 8, 11, 208, 4, 32, 8, 144, 40, 84, 8, 80, 8, 144, 4, 32, 11, 208, 19, 200, 2, 64, 1, 128, 0, 0, 0, 0},
    
  //ambient light level 40
  {0, 0, 4, 62, 32, 34, 19, 238, 4, 34, 8, 46, 80, 34, 16, 46, 16, 34, 80, 46, 8, 34, 4, 46, 19, 226, 32, 46, 2, 62, 0, 0},
  
  //no auto open 41
  {0, 0, 0, 0, 63, 224, 32, 32, 34, 32, 39, 32, 47, 160, 32, 32, 40, 224, 33, 16, 34, 56, 34, 104, 34, 200, 63, 144, 0, 224, 0, 0},
  //auto open light level 42
  { 0, 0, 0, 0, 63, 224, 34, 32, 39, 32, 47, 160, 32, 32, 32, 224, 41, 16, 34, 72, 34, 168, 34, 232, 34, 168, 63, 16, 0, 224, 0, 0},
  //auto open timed 43
  {0, 0, 0, 0, 63, 224, 34, 32, 39, 32, 47, 160, 32, 32, 32, 224, 41, 16, 34, 72, 34, 72, 34, 120, 34, 8, 63, 16, 0, 224, 0, 0},
  //no auto close 44
  {0, 0, 0, 0, 63, 224, 32, 32, 47, 160, 39, 32, 34, 32, 32, 32, 40, 224, 33, 16, 34, 56, 34, 104, 34, 200, 63, 144, 0, 224, 0, 0},
  //auto close light level 45
  {0, 0, 0, 0, 63, 224, 32, 32, 47, 160, 39, 32, 34, 32, 32, 224, 41, 16, 34, 72, 34, 168, 34, 232, 34, 168, 63, 16, 0, 224, 0, 0},
  //auto close timed 46
  { 0, 0, 0, 0, 63, 224, 32, 32, 47, 160, 39, 32, 34, 32, 32, 224, 41, 16, 34, 72, 34, 72, 34, 120, 34, 8, 63, 16, 0, 224, 0, 0},
  
  //autodoor 1 47
  {0, 0, 0, 0, 63, 224, 32, 32, 36, 32, 44, 32, 36, 112, 36, 136, 47, 36, 33, 84, 33, 116, 33, 84, 63, 136, 0, 112, 0, 0, 0, 0},
  //autodoor 2 48
  {0, 0, 0, 0, 63, 224, 32, 32, 38, 32, 43, 32, 36, 112, 40, 136, 47, 36, 33, 84, 33, 116, 33, 84, 63, 136, 0, 112, 0, 0, 0, 0},
  
  
  //current time 49
  {0, 0, 0, 0, 3, 192, 15, 240, 28, 56, 25, 152, 49, 140, 49, 236, 49, 236, 50, 12, 24, 24, 28, 56, 15, 240, 3, 192, 0, 0, 0, 0},
  //close time 50
  {0, 0, 0, 0, 63, 224, 32, 32, 47, 160, 39, 32, 34, 32, 32, 224, 41, 16, 34, 72, 34, 72, 34, 120, 34, 8, 63, 16, 0, 224, 0, 0},
  //open time 51
  {0, 0, 0, 0, 63, 224, 34, 32, 39, 32, 47, 160, 32, 32, 32, 224, 41, 16, 34, 72, 34, 72, 34, 120, 34, 8, 63, 16, 0, 224, 0, 0},
  //close light level 52
  { 0, 0, 4, 62, 32, 34, 19, 238, 4, 34, 8, 46, 80, 34, 23, 238, 19, 162, 81, 46, 8, 34, 4, 46, 19, 226, 32, 46, 2, 62, 0, 0},
  //open light level 53
  {0, 0, 4, 62, 32, 34, 19, 238, 4, 34, 8, 46, 81, 34, 19, 174, 23, 226, 80, 46, 8, 34, 4, 46, 19, 226, 32, 46, 2, 62, 0, 0}
};

//small number font
#define FONT_NUM 10
#define FONT_SIZE 6
#define FONT_OFFSET NUM_TO_SAVE*ICON_SIZE
#define SMALL_FONT_OFFSET (NUM_TO_SAVE*ICON_SIZE)+(FONT_NUM*FONT_SIZE)
static const unsigned char font[FONT_NUM][FONT_SIZE] PROGMEM={
  {96, 144, 176, 208, 144, 96}, //0
  {32, 96, 32, 32, 32, 112},    //1
  {96, 144, 16, 32, 64, 240},   //2
  {96, 144, 32, 16, 144, 96},   //3
  {32, 96, 160, 240, 32, 32},   //4
  {240, 128, 224, 16, 16, 224}, //5
  {112, 128, 224, 144, 144, 96},//6
  {240, 16, 32, 64, 64, 64},    //7
  {96, 144, 96, 144, 144, 96},  //8
  {96, 144, 144, 112, 16, 96},  //9
};

static const unsigned char smallFont[FONT_NUM][FONT_SIZE] PROGMEM={
  {64, 160, 160, 160, 160, 64}, //0
  {64, 192, 64, 64, 64, 224},    //1
  {64, 160, 32, 64, 128, 224},   //2
  {224, 32, 64, 32, 32, 192},   //3
  {32, 96, 160, 224, 32, 32},   //4
  {224, 128, 224, 32, 32, 192}, //5
  {96, 128, 192, 160, 160, 64},//6
  {224, 32, 32, 64, 64, 64},    //7
  {64, 160, 64, 160, 160, 64},  //8
  {64, 160, 160, 96, 32, 64},  //9
};
//door open
/*static const unsigned char bmp1[32]={
     127, 254, 128, 1, 143, 241, 137, 17, 137, 17, 137, 17, 137, 17, 137, 145, 137, 17, 137, 17, 137, 17, 137, 145, 136, 113, 136, 17, 128, 1, 127, 254
};*/

//door closing
/*static const unsigned char bmp1[32]={
      127, 254, 128, 1, 143, 241, 136, 17, 136, 17, 136, 17, 136, 17, 138, 17, 136, 17, 136, 17, 136, 17, 136, 17, 136, 17, 143, 241, 128, 1, 127, 254
};*/

//door Opening
/*static const unsigned char bmp2[32]={
      127, 254, 128, 1, 191, 193, 162, 65, 162, 65, 163, 65, 162, 225, 163, 25, 162, 73, 164, 69, 164, 117, 164, 133, 162, 9, 163, 25, 128, 225, 127, 254
};
*/

//door closed
/*
static const unsigned char bmp1[32]={
 127, 254, 128, 1, 143, 241, 136, 17, 136, 17, 136, 17, 136, 17, 138, 17, 136, 17, 136, 17, 136, 17, 136, 17, 136, 17, 143, 241, 128, 1, 127, 254
 };
 */
 
//Check Mark
/*static const unsigned char check[32] PROGMEM ={
  127, 254, 128, 1, 128, 1, 128, 9, 128, 29, 128, 57, 140, 113, 156, 225, 189, 193, 191, 129, 159, 1, 143, 1, 134, 1, 128, 1, 128, 1, 127, 254};
*/
unsigned char tempIcon[32];

unsigned int offsets[5];

//================================================================

void setup(){
  Serial.begin(9600);
  Serial.print("Start...");
  
  //offsets[0] = EEPROMIO.loadInit(autoOpenTimeHours);
  //offsets[1] = EEPROMIO.loadInit(autoOpenTimeMinutes); 
  //offsets[2] = EEPROMIO.loadInit(autoCloseTimeHours); 
  //offsets[3] = EEPROMIO.loadInit(autoCloseTimeMinutes);
  //offsets[4] = EEPROMIO.loadInit(iconsUsed);
  //offsets[4] = EEPROMIO.loadInitArray(icon, NUM_ICONS);

  //only set iconsUsed to 0 if it's the first run
  //iconsUsed=0;

  if (flash.initialize()){
    Serial.println("Init OK!");
    flash.chipErase();
    while(flash.busy());
    for(int i=0;i<NUM_TO_SAVE;++i){
      saveIcons(i);
    }
    
    saveFont();
    
  }else{
    Serial.println("Init FAIL!");
  }
  
  //prep the display
  display.begin();
  display.clearDisplay();
  display.setContrast(50);
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setTextWrap(false);
  

  
  /*
  flash.readBytes(0,tempIcon,32);
  while(flash.busy());
  Serial.println("icon1");
  //writeIconBytes(tempIcon);
  drawBitmap(30,0,tempIcon, 16, 16,1);

  flash.readBytes(32,tempIcon,32); 
  while(flash.busy());
  Serial.println("");
  Serial.println("icon2");
  //writeIconBytes(tempIcon);
  drawBitmap(30,16,tempIcon, 16, 16,1);
  
  //display.drawBitmap(0,0,bmp2, 16, 16,1);
  //display.drawBitmap(0,16,bmp1, 16, 16,1);
  
  display.drawBitmap(0,0,check,16,16,1);
  readProgmemIcon(tempIcon,check);
  drawBitmap(0,16,tempIcon,16,16,1);
  
  drawBitmap(50,0,bmp1,16,16,1);
  drawBitmap(50,16,bmp2,16,16,1);
  display.display();
  */
  delay(1000);
}

//================================================================

void saveIcons(int iconNum){ 
  for(int i=0;i<NUM_TO_SAVE;++i){
    readProgmemIcon(tempIcon,icons[i]);
    flash.writeBytes(i*32,tempIcon,32);
    while(flash.busy());
  }
}

void readProgmemIcon(unsigned char icon[], const unsigned char* pMemIcon){
  for(int i=0;i<32;++i){  
    icon[i]= pgm_read_byte_near(pMemIcon + i);
  }
}

void saveFont(){
  unsigned char letter[FONT_SIZE];
  for(int i=0;i<FONT_NUM;++i){
    readProgmemFont(letter, font[i]);
    flash.writeBytes(FONT_OFFSET+i*FONT_SIZE, letter, FONT_SIZE);
    while(flash.busy());
  }
  
  for(int i=0;i<FONT_NUM;++i){
    readProgmemFont(letter, smallFont[i]);
    flash.writeBytes(SMALL_FONT_OFFSET+i*FONT_SIZE, letter, FONT_SIZE);
    while(flash.busy());
  }
}

void readProgmemFont(unsigned char character[], const unsigned char* pMemChar){
  for(int i=0;i<FONT_SIZE;++i){  
    character[i]= pgm_read_byte_near(pMemChar + i);
  }
}

void drawBitmap(uint16_t x,uint16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color){
  //for each bit set in the byte array, draw a pixel
  int bytesPerRow = w/8;
  if(w%8!=0){
    bytesPerRow++;
  }
  
  for(int i=0;i< h;++i){
    for(int j=0;j<w;++j){
      
      //get the byte we are looking for
      int rowStart=i*bytesPerRow;
      uint8_t byteVal = bitmap[rowStart+j/8];
      
      //get the bit we are looking for
      
      int bitLoc = 7-(j%8);
      int bitVal= (byteVal >> bitLoc) & 0x01;
      if(bitVal!=0){
        display.drawPixel(x+j, y+i, color);
      }
    }
  }
}

//================================================================

void loop(){
    //display icons from flash one at a time
    unsigned char letter[FONT_SIZE];
  for(int i=0;i<NUM_TO_SAVE;++i){
    display.clearDisplay();
    flash.readBytes(32*i,tempIcon,32);
    while(flash.busy());
    
    flash.readBytes(SMALL_FONT_OFFSET+((i%10)*FONT_SIZE),letter,FONT_SIZE);
    while(flash.busy());
    
    display.println(i);
    display.drawRoundRect(20, 0, 16, 16, 1, 1);
    //fillRoundRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t radius, uint16_t color);
    drawBitmap(20,0,tempIcon, 16, 16,1);
    
    drawBitmap(40,0,letter,5,6,1);
    
    display.display();
    delay(1000);
  }
}




