#ifndef EEPROMIO_H
#define EEPROMIO_H

#include <inttypes.h>

class EepromIoClass{ 
  public:
  
	unsigned int eeadx;
  
	uint8_t read(int);
    void write(int address, uint8_t value);	
	int saveString(char* str, int size, int address);
	int loadString(char* str, int size, int address);

	//========================================================================================================
	
	template <class T> unsigned int save(const T& value, unsigned int address){
	   const byte* p = (const byte*)(const void*)&value;
	   int i;
	   for (i = 0; i < sizeof(value); i++){
		   write(address++, *p++);
	   }
	   return i;
	}

	template <class T> unsigned int load(T& value, unsigned int address){
	   byte* p = (byte*)(void*)&value;
	   int i;
	   for (i = 0; i < sizeof(value); i++){
		   *p++ = read(address++);
	   }
	   return i;
	}
	
	//========================================================================================================
	// Initialization
	//========================================================================================================
	
	void init(unsigned int offset){
		eeadx = offset;	
	}
	
		//establishes the address list on startup
	template <class T> unsigned int loadInit(T& value){
		unsigned int temp = eeadx;
		eeadx+=load(value, eeadx);
		return temp;
	}
	
    //establishes the address list on startup
	template <class T> unsigned int loadInitArray(T& value, unsigned int size){
		unsigned int temp = loadInit(value);
		for(int i = 1; i < size; ++i){
			loadInit(value);  
		}		
		return temp;
	}
	
	//establishes the address list on startup and initializes the array
	template <class T> unsigned int loadInitArrayValues(T value[], unsigned int size){
		unsigned int temp = loadInit(value[0]);
		for(int i = 1; i < size; ++i){
			loadInit(value[i]);  
		}		
		return temp;
	}
	
	//establishes the address list on startup
	unsigned int loadInitString(char* str, int maxSize){
		unsigned int temp = eeadx;
		for(int i=0; i < maxSize; ++i){
			str[i] = (char)read(eeadx++);
			//if(str[i]=='\0'){
				//++i;
			//	break;
			//}
		}	
		eeadx++;
		//if(i>=maxSize){
			//if the max size limit was reached, ensure the  
			//string is terminated with a null character
		//	str[maxSize]='\0';
		//}	
		return temp;
	}
	
	//========================================================================================================
	// Loaders
	//========================================================================================================
	
	template <class T> void loadFromOffset(T& value, unsigned int offset){
		load(value, offset);
	}
	
	//value = item to load arrayIndex = item index offset = memory offset for the array
	template <class T> void loadFromIndexedArray(T& value, unsigned int arrayIndex, unsigned int offset){
		unsigned int address = (sizeof(value) * arrayIndex) + offset;
		load(value, address);
	}
	
	
	
	//========================================================================================================
	//Save and Update
	//========================================================================================================
	
	/*template <class T> unsigned int saveNew(const T& value){
		//TODO: check that max number of address indexes have not been exceeded
		unsigned int ret = eeadx;
		eeadx+=save(value, eeadx);
		return ret;		
	}*/
	
	template <class T> void updateIndexData(const T& value, unsigned int offset){
		//TODO: currently need to save something of exact size as original
		//TODO:  check that memory available is/was not exceeded
		save(value, offset);	
	}
	
	template <class T> void updateIndexedArray(const T& value, unsigned int indexIntoArray, unsigned int offset){
		unsigned int address = (sizeof(value) * indexIntoArray) + offset;
		save(value, address);		
	}
	
};

extern EepromIoClass EEPROMIO;


#endif