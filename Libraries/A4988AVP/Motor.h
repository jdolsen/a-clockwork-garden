#ifndef MOTOR_H
#define MOTOR_H

#include "movEl.h"

typedef struct Motor{
  char stepPin;
  char directionPin;
  char enablePin;
  char doEnableDisable;
  char direction;
  
  char* notificationFlag; //pointer to the flag polled by the main thread
                                      //Note that this reduces accuracy, but it maintains
                                      //simplicity for the ISR function
  volatile int notificationSteps; //if non-zero, decrement every step.  when zero, increment the flag
  int recurringNotificationSteps; //saves notification steps for recurring notifications
  
  volatile long position;
  long maxPosition;
  
  volatile char decelerating;
  long acceleration;
  long instAcceleration;
  long positionToStartDecel;
  long positionToStop;
  volatile long accelPulseCount;
  int ticksPerVelocityChange;
  
  long velocity;
  volatile long instVelocity;
  long maxVelocity;
  volatile char count; //a raw count from the timer
  volatile int pulseNum;
  volatile int ticksPerPulse;
  volatile int finishedFlag;
  char isContinuous;

} Motor;

#endif