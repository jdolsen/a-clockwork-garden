#include "A4988AVP.h"
#include "Arduino.h"

 
//---------------------------------------------------------------------------------------

 A4988AVP::A4988AVP(){
	
	for(int i=0;i<NUM_MOTORS;++i){
		motors[i].velocity=0;
		motors[i].maxVelocity=MAX_NUM_PULSES;

		motors[i].count=0;
		motors[i].pulseNum=0;
		motors[i].ticksPerPulse=0;
		
		motors[i].position=0;
		motors[i].finishedFlag=0;
		motors[i].instAcceleration=0;
		motors[i].isContinuous=0;
		
		motors[i].notificationFlag=0;
		motors[i].notificationSteps=0;
		motors[i].recurringNotificationSteps=0;
	}
		
}

//---------------------------------------------------------------------------------------

ISR(TIMER1_COMPA_vect){//timer2 interrupt 1kHz toggles pin 9 at 2Hz
	Driver.doISR();
}

void A4988AVP::initInt(){
		//interrupt every 200uscs (5KHz) (2.5KHz max pulse rate) this is the maximum pulse width for these steppers
	cli();
	TCCR1A = 0;// set entire TCCR1A register to 0
	TCCR1B = 0;// same for TCCR1B
	TCNT1  = 0;//initialize counter value to 0
	// set compare match register for 5kHz increments
	OCR1A = 399;//499;// = (16*10^6) / (5000*64) - 1 (must be <65536 for timer 1)	  
	TCCR1B |= (1 << WGM12); // turn on CTC mode	  
	TCCR1B |= (1 << CS11); // Set CS11 bit for 8 prescaler  
	TIMSK1 |= (1 << OCIE1A);// enable timer compare interrupt
	sei();
}

void A4988AVP::initMotor(char sPin, char dPin, char ePin, int motor){
	pinMode(sPin, OUTPUT);
	pinMode(dPin, OUTPUT);
	pinMode(ePin, OUTPUT);
	
	motors[motor].stepPin=sPin;
	motors[motor].directionPin=dPin;
	motors[motor].enablePin=ePin;
		
	digitalWrite(motors[motor].directionPin,0);
	digitalWrite(motors[motor].stepPin,0);	 
	digitalWrite(motors[motor].enablePin,1);
	motors[motor].doEnableDisable=AUTO_ENABLE;
	autoDisableMotor(motor);
 }
 
 
 void A4988AVP::initMotor(char sPin, char dPin, int motor){
	pinMode(sPin, OUTPUT);
	pinMode(dPin, OUTPUT);
	
	motors[motor].stepPin=sPin;
	motors[motor].directionPin=dPin;
	
	digitalWrite(motors[motor].directionPin,0);
	digitalWrite(motors[motor].stepPin,0);	 
	motors[motor].doEnableDisable=NO_ENABLE;
 }

//---------------------------------------------------------------------------------------

void A4988AVP::doISR(){
	//every other tick, increment the pulses and toggle the step pulse line if ticks per pulse has been reached
	//update steps per second, velocity, and acceleration	

	for(int i=0;i<NUM_MOTORS;++i){	
		motors[i].count++;
		if(motors[i].count%2==1 && motors[i].finishedFlag==0){	
			//update velocity based on acceleration value
			if(motors[i].acceleration!=0){
				motors[i].accelPulseCount++;
				
				//adjust velocity based on acceleration and other parameters
				if(motors[i].accelPulseCount>=motors[i].ticksPerVelocityChange){
					motors[i].accelPulseCount=0;
					
					//do the adjustment
					if(motors[i].decelerating==1){
						//always decrement velocity if in the decelerating phase
						//no need to worry about negative values since they indicate done and are handled below
						motors[i].instVelocity-=motors[i].instAcceleration;
					}else{
						if(motors[i].velocity!=0){//seek the currently set velocity
							if(motors[i].instVelocity<motors[i].velocity){
								motors[i].instVelocity+=motors[i].instAcceleration;
								if(motors[i].instVelocity>motors[i].velocity){
									motors[i].instVelocity=motors[i].velocity;
								}
							}else if(motors[i].instVelocity==motors[i].velocity){
								//do nothing
							}else{//inst velocity is greater than velocity, decrement until new velocity reached
								//TODO: test that this doesn't cause lost steps in certain 
								//low velocity situations, or when dealing with deceleration phase transitions
								//during subsequent changes to set velocity 
								motors[i].instVelocity-=motors[i].instAcceleration;
								if(motors[i].instVelocity<motors[i].velocity){
									motors[i].instVelocity=motors[i].velocity;
								}
							}
						}else{//acceleration only - go up to the max system value (set by MAX_NUM_PULSES)
							motors[i].instVelocity+=motors[i].instAcceleration;
							if(motors[i].instVelocity>=motors[i].maxVelocity){
								motors[i].instVelocity=motors[i].maxVelocity;
							}
						}
					}
		
					//recalculate ticks per pulse based on new velocity
					if(motors[i].decelerating==0){//do not recalculate velocity here during deceleration 
										   //(do when a pulse is sent - this is due to the difference between
										   //declerating and accelerating phases)
						if(motors[i].instVelocity!=0){
							motors[i].ticksPerPulse=MAX_NUM_PULSES/motors[i].instVelocity;
						}else{
							motors[i].ticksPerPulse=0;//don't move if the velocity is zero
						}						
					}
					
				}
			}//end acceleration handling
			
			//determine if pulse should be sent (advance a step) based on velocity
			motors[i].pulseNum++;//pulseNum must never be more than MAX_NUM_PULSES
			if(motors[i].pulseNum>=motors[i].ticksPerPulse && motors[i].ticksPerPulse!=0 && 
			  (motors[i].position!=motors[i].positionToStop || motors[i].isContinuous==1)){
				//this is for the benefit of the decelerating phase when ticks per pulse is not calculated above
				if(motors[i].instVelocity!=0){
					motors[i].ticksPerPulse=MAX_NUM_PULSES/motors[i].instVelocity;
				}else{
					motors[i].ticksPerPulse=0;//don't move if the velocity is zero
				}
				
				//send a step pulse
				digitalWrite(motors[i].stepPin,1);
				
				//update position
				if(motors[i].direction==1 || motors[i].isContinuous==1){
					//increment position (always increment in continuous mode)
					motors[i].position++;
				}else{
					//decrement position
					motors[i].position--;
				}
				
				//determine if notifications should be triggered
				if(motors[i].notificationSteps!=0){
					motors[i].notificationSteps-=1;
					if(motors[i].notificationSteps<=0){
						//notify
						char* tempFlag = motors[i].notificationFlag;
						*tempFlag+=1;
						
						//if notification is recurring, reset notificationSteps
						if(motors[i].recurringNotificationSteps!=0){
							motors[i].notificationSteps=motors[i].recurringNotificationSteps;
						}
					}		
				}
				
				motors[i].pulseNum=0;
			}//end send a step high pulse	

			//determine if deceleration	should begin (don't auto-decelerate in continuous mode (isContinuous==1))
			if(motors[i].position == motors[i].positionToStartDecel && motors[i].isContinuous==0){			
				motors[i].decelerating=1;
			}
		
			//determine if end conditions are met
			if((motors[i].position == motors[i].positionToStop && motors[i].isContinuous == 0) || 
			   (motors[i].instVelocity<=0 && motors[i].decelerating==1 && motors[i].isContinuous==1)){ //Ensure velocity 
													//will NEVER reach 0.  If velocity ever reached 0 before steps were 
													//complete this would not return. 
													//added instVelocity term to conditional for decelerate from continuous mode
													
				motors[i].instVelocity = motors[i].velocity;      //when the steps reaches zero, reset the velocity
				motors[i].pulseNum=0;
				motors[i].finishedFlag=1;		
			}
		
		}else{
			motors[i].count=0;
			//set step pulse line to 0
			digitalWrite(motors[i].stepPin,0);
		}
	}
}

//---------------------------------------------------------------------------------------

char A4988AVP::setVelocity(long stepsPerSecond,int motor){
		
	motors[motor].velocity = stepsPerSecond;	
	char ret=0;
	if(motors[motor].velocity<0){
		motors[motor].velocity=-motors[motor].velocity;		
	}
	
	if(motors[motor].velocity>motors[motor].maxVelocity){
		motors[motor].velocity=motors[motor].maxVelocity;
		ret=1;
	}

	if(motors[motor].velocity!=0){
		motors[motor].ticksPerPulse=motors[motor].maxVelocity/motors[motor].velocity;
	}else{
		motors[motor].ticksPerPulse=0;//don't move if the velocity is zero
	}	
	
	motors[motor].instVelocity=motors[motor].velocity;
	return ret;
}

//---------------------------------------------------------------------------------------

char A4988AVP::setMaxVelocity(long stepsPerSecond,int motor){
	motors[motor].maxVelocity = stepsPerSecond;
	char ret=0;
	//negative velocities are not allowed
	if(motors[motor].maxVelocity<0){
		motors[motor].maxVelocity=-motors[motor].maxVelocity;
	}
	
	if(motors[motor].velocity>MAX_NUM_PULSES){
		motors[motor].velocity=MAX_NUM_PULSES;
		ret=1;
	}
	
	//ensure the velocity is not greater than MAX_NUM_PULSES
	if(motors[motor].maxVelocity > MAX_NUM_PULSES){
		motors[motor].maxVelocity = MAX_NUM_PULSES;
	}
	return ret;
}

//---------------------------------------------------------------------------------------

void A4988AVP::setAcceleration(long stepsPerSecond, int motor){
	motors[motor].decelerating = 0;

	motors[motor].acceleration = stepsPerSecond;
	if(motors[motor].acceleration<0){
		motors[motor].acceleration=-motors[motor].acceleration;
	}
	long incrPerPulse = motors[motor].acceleration/MAX_NUM_PULSES;
	if(incrPerPulse!=0){
		motors[motor].ticksPerVelocityChange=1;
		motors[motor].instAcceleration=incrPerPulse;
	}else{
		if(motors[motor].acceleration!=0){
			motors[motor].ticksPerVelocityChange = MAX_NUM_PULSES/motors[motor].acceleration;
			motors[motor].instAcceleration=1;					
		}
	}
}

//---------------------------------------------------------------------------------------

void  A4988AVP::setMaxPosition(long pos, int motor){
	motors[motor].maxPosition=pos;
}

//---------------------------------------------------------------------------------------

long  A4988AVP::getPosition(int motor){
	return motors[motor].position;
}

//---------------------------------------------------------------------------------------

bool A4988AVP::gotoPosition(long pos, int motor){

	return doSteps(pos-motors[motor].position,motor);	
}

//---------------------------------------------------------------------------------------

void A4988AVP::setPosition(long pos, int motor){
	motors[motor].position=pos;	
}

//---------------------------------------------------------------------------------------

bool A4988AVP::doSteps(long steps,int motor){
	if(motors[motor].finishedFlag==1){
		autoEnableMotor(motor);	
		motors[motor].decelerating=0;
		
	}
	
	long accelSteps=0;
	//keep track of total steps for continuous mode
	if(motors[motor].isContinuous==1){
		motors[motor].position=0;
	}
	
	if(steps>0){
		accelSteps = determineAccelerationSteps(steps, motor);
		motors[motor].positionToStartDecel = motors[motor].position+(steps-accelSteps);
		digitalWrite(motors[motor].directionPin,1);	
		motors[motor].direction=1;
	}else{
		accelSteps = determineAccelerationSteps(-steps, motor);
		motors[motor].positionToStartDecel = motors[motor].position-(-steps-accelSteps);
		digitalWrite(motors[motor].directionPin,0);
		motors[motor].direction=0;
	}
	
	//determine the ending position of the motor and start moving
	long endPosition = motors[motor].position+steps;
	if((motors[motor].maxPosition!=0 && endPosition>motors[motor].maxPosition) || endPosition<0){
		//do not start the motor since it would overextend its range
		motors[motor].finishedFlag=1;
		return false;
		
	}else{		
		motors[motor].positionToStop=endPosition;
		motors[motor].finishedFlag=0;
		return true;
		
	}
}

//---------------------------------------------------------------------------------------

//returns any steps remaining
long A4988AVP::waitForSteps(long steps,int motor){
	autoEnableMotor(motor);
	motors[motor].decelerating=0;
	long accelSteps=0;
	
	if(steps>0){
		accelSteps = determineAccelerationSteps(steps, motor);
		motors[motor].positionToStartDecel = motors[motor].position+(steps-accelSteps);
		digitalWrite(motors[motor].directionPin,1);	
		motors[motor].direction=1;		
	}else{
		accelSteps = determineAccelerationSteps(-steps, motor);
		motors[motor].positionToStartDecel = motors[motor].position-(-steps-accelSteps);
		digitalWrite(motors[motor].directionPin,0);
		motors[motor].direction=0;
	}

	//determine the ending position of the motor
	long endPosition = motors[motor].position+steps;
	if((motors[motor].maxPosition!=0 && endPosition>motors[motor].maxPosition) || endPosition<0){
		//do not start the motor since it would overextend its range
		motors[motor].finishedFlag=1;
		return 0;		
	}else{
		motors[motor].positionToStop=endPosition;		
	}
	
	motors[motor].finishedFlag=0;
	while(motors[motor].finishedFlag==0);
	autoDisableMotor(motor);
	return motors[motor].position;
}
 
//---------------------------------------------------------------------------------------

long A4988AVP::determineAccelerationSteps(int steps,int motor){
	//determines the number of steps it takes to decelerate or accelerate (to/from 0 to from maxVelocity)
	//by simulating the acceleration process
	int pulses = 1;
	long stepsDone = 0;
	int prevStepPulse = 0;
	long currentVelocity=0;
	long pulsesUntilStep=0;

	
	while(1){ 		
		currentVelocity = (pulses * motors[motor].acceleration)/MAX_NUM_PULSES;
		pulsesUntilStep = prevStepPulse+ (MAX_NUM_PULSES/currentVelocity);
		if(pulses>=pulsesUntilStep){
			prevStepPulse=pulses;
			stepsDone++;
		}
	  
		
		if(stepsDone >=steps/2||currentVelocity>=motors[motor].maxVelocity){
			//stepsToDecelerate[motor] = stepsDone;
			return stepsDone;
		}
		pulses++;
	}
}

//---------------------------------------------------------------------------------------

//will return the change of position since called
long  A4988AVP::waitForMotorFinished(int motor){
	long temp = motors[motor].position;	
	while(motors[motor].finishedFlag==0);
	
	autoDisableMotor(motor);
	temp=motors[motor].position-temp;
	return motors[motor].position-temp;
}

//---------------------------------------------------------------------------------------

void A4988AVP::startContinuous(char dir, int motor){
	digitalWrite(motors[motor].directionPin,dir);
	motors[motor].direction=dir;
	autoEnableMotor(motor);
	motors[motor].decelerating = 0;
	motors[motor].position=0;
	motors[motor].isContinuous=1;
	motors[motor].finishedFlag=0;
}

//---------------------------------------------------------------------------------------

int A4988AVP::decelerateContinuous(int motor){
	
	if(motors[motor].finishedFlag==1){
		//do nothing
	}else{
		//begin and wait for motor deceleration
		
		motors[motor].decelerating=1;
		//finishedFlag[motor]=0;
		
		//wait for deceleration to finish and return the total position count
		while(motors[motor].finishedFlag==0);
		
	}
	//stop the motor
	motors[motor].pulseNum=0;
	motors[motor].isContinuous=0;
	
	motors[motor].positionToStop=motors[motor].position;
	motors[motor].finishedFlag=1;
	autoDisableMotor(motor);
	//return total step count
	return motors[motor].position;
}

//---------------------------------------------------------------------------------------

long A4988AVP::stopMotor(int motor){
	//stop the motor1
	motors[motor].pulseNum=0;
	motors[motor].isContinuous=0;
	
	//positionToStop[motor]=position[motor];
	motors[motor].finishedFlag=1;
	autoDisableMotor(motor);
	//return total step count
	return motors[motor].position;
}

//---------------------------------------------------------------------------------------

void A4988AVP::initRPM(int stepsPerRotation, int microstepsPerStep){
	RPMFactor = stepsPerRotation*microstepsPerStep;
}

//---------------------------------------------------------------------------------------

void  A4988AVP::setRPMAcceleration(double RPM2,int motor){
	long steps = RPM2*RPMFactor;
	setAcceleration(steps,motor);
}

//---------------------------------------------------------------------------------------

char  A4988AVP::setRPMVelocity(double RPM,int motor){
	long steps = RPM*RPMFactor;
	return setVelocity(steps,motor);
}

//---------------------------------------------------------------------------------------

char  A4988AVP::setRPMMaxVelocity(double RPM,int motor){
	long steps = RPM*RPMFactor;
	return setMaxVelocity(steps,motor);
}

//---------------------------------------------------------------------------------------

void  A4988AVP::doRotations(double RPM,int motor){
	long steps = RPM*RPMFactor;
	doSteps(steps,motor);
}

//---------------------------------------------------------------------------------------

int  A4988AVP::waitForRotations(double RPM,int motor){
	long steps = RPM*RPMFactor;
	return waitForSteps(steps,motor);
}

//---------------------------------------------------------------------------------------

void A4988AVP::notifyAfterSteps(long steps, char* flag, char isRecurring, int motor){
	motors[motor].notificationSteps=steps;
	motors[motor].notificationFlag=flag;
	if(isRecurring!=0){
		motors[motor].recurringNotificationSteps=steps;
	}else{
		motors[motor].recurringNotificationSteps=0;
	}
		
}

//---------------------------------------------------------------------------------------

void A4988AVP::endNotify(int motor){
	motors[motor].notificationSteps=0;
	motors[motor].recurringNotificationSteps=0;
}

//---------------------------------------------------------------------------------------

//private methods
void  A4988AVP::autoEnableMotor(int motor){
	if(motors[motor].doEnableDisable==NO_ENABLE || motors[motor].doEnableDisable==ENABLE_CAPABLE){
		//do nothing since autoenabled is not set
	}else{
		digitalWrite(motors[motor].enablePin,0);
	}
}

void  A4988AVP::autoDisableMotor(int motor){
	if(motors[motor].doEnableDisable==NO_ENABLE || motors[motor].doEnableDisable==ENABLE_CAPABLE){
		//do nothing since autoenabled is not set
	}else{
		digitalWrite(motors[motor].enablePin,1);
	}
}

//public methods
void  A4988AVP::enableMotor(int motor){
	if(motors[motor].doEnableDisable==NO_ENABLE){
		//do nothing
	}else{
		digitalWrite(motors[motor].enablePin,0);
	}
}

void  A4988AVP::disableMotor(int motor){
	if(motors[motor].doEnableDisable==NO_ENABLE){
		//do nothing
	}else{
		digitalWrite(motors[motor].enablePin,1);
	}
}

 void A4988AVP::setEnableDisable(char autoVal, int motor){
	if(motors[motor].doEnableDisable==NO_ENABLE || autoVal==NO_ENABLE){
		//do not change to or from the NO_ENABLE state
	}else{
	  motors[motor].doEnableDisable=autoVal;
	}
 }
  
A4988AVP Driver = A4988AVP();