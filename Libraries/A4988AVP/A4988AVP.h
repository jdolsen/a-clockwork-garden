#ifndef A4988_AVP_H
#define A4988_AVP_H

#define MAX_NUM_PULSES 2500 //2000
#include "arduino.h"
#include "Motor.h"
#define NO_ENABLE 0
#define ENABLE_CAPABLE 1
#define AUTO_ENABLE 2

#define NUM_MOTORS 5
class A4988AVP{
  private:
  
  Motor motors[NUM_MOTORS];
  

  long RPMFactor;
  
  long determineAccelerationSteps(int steps, int motor);
  void  autoEnableMotor(int motor);
  void  autoDisableMotor(int motor);

  
  public:
  //initialization
  A4988AVP();
  void initInt();
  void initMotor(char sPin, char dPin, char ePin, int motor);
  void initMotor(char sPin, char dPin, int motor);
  void doISR();
  
  //acceleration
  void setAcceleration(long stepsPerSecond,int motor);
  char setVelocity(long stepsPerSecond,int motor);
  char setMaxVelocity(long stepsPerSecond,int motor);
  
  //position functions
  void setPosition(long pos, int motor);//sets the current position (mostly used to zero the position)
  void setMaxPosition(long position, int motor);
  long getPosition(int motor);
  
  //movement functions
  bool gotoPosition(long position, int motor);//calculates steps to get to the new position and uses
											  //doSteps to move the motor
  bool doSteps(long steps,int motor);//begins motor movement and returns
  long waitForSteps(long steps,int motor);//waits for the motor to complete movement
  long waitForMotorFinished(int motor);//begins motor movement, waits for completion
									   //and returns
  long stopMotor(int motor); //returns position
  
  //Notification functions
  void notifyAfterSteps(long steps, char* flag, char isRecurring, int motor);
  void endNotify(int motor);
  
  //Continuous Mode functions
  void startContinuous(char direction, int motor);
  int decelerateContinuous(int motor);//returns steps remaining
  
  //Enable/disable functions
  void  enableMotor(int motor);
  void  disableMotor(int motor);
  void  setEnableDisable(char isAuto, int motor);
  
  //RPM functions
  void initRPM(int stepsPerRotation, int microstepsPerStep);
  void setRPMAcceleration(double RPM2,int motor);
  char setRPMVelocity(double RPM,int motor);
  char setRPMMaxVelocity(double RPM,int motor);
  void doRotations(double RPM,int motor);
  int waitForRotations(double RPM,int motor);
  
};
extern A4988AVP Driver;
#endif