#include "EventQueue.h"

EventQueue::EventQueue(){
}

void EventQueue::initEvents(QueueElement* array, const int& size, bool* qEnable){
  elements = array;
  queueSize = size;
  queueingEnabled=qEnable;

  for(int i = 0; i<size; ++i){
    array[i].isValid=false;
  } 
} 

//=====================================================================

//Events may be queued up to 65 seconds into the future
bool EventQueue::addQueueEvent(QueueFunction fun, unsigned int delayMs, int n_args, ...){
  //add the event to the first open slot
  if(!(*queueingEnabled)){
    return false;
  }
  boolean isAvailable = false;
  for(int i = 0; i<queueSize; ++i){
    if(elements[i].isValid==false){	  
      elements[i].function = fun;
      elements[i].delayMs = delayMs;
      elements[i].isValid = 1;
	  
	  va_list ap;
	  va_start(ap, n_args); 
	  for(int j = 0; j < n_args && j <NUM_QUEUE_ELEMENT_VALUES; j++) {
        elements[i].values[j] = va_arg ( ap, int );
	  }
      va_end(ap);
	  
      isAvailable = true;
	  
      break;
    }//endif
  }//endfor
  
  return isAvailable;
}

//=====================================================================

bool EventQueue::executeFirstValidEvent(unsigned int timeSinceLastMs){
  boolean executed = false;
  for(int i = 0; i<queueSize; ++i){
     //only execute valid events
     if(elements[i].isValid==1){
       //only execute the first event with an expired event time
       if(elements[i].delayMs <=0 && !executed){
	     executed = true;//TODO: don't use this boolean, just break the loop
         elements[i].function(elements[i].values);
         elements[i].isValid = 0; 
         
       }else{
          //decrement the wait time for all other valid events
		 if(elements[i].delayMs < timeSinceLastMs){
			elements[i].delayMs = 0;
		 }else{
			 elements[i].delayMs = elements[i].delayMs - timeSinceLastMs;
         }
       }//endif-else
     }//endif valid      
  }//endfor
  return executed;
}

//=====================================================================

unsigned int EventQueue::getTimeUntilNextEvent(){
	unsigned int response=65535;//maximum size of the delay value
	for(int i = 0; i<queueSize; ++i){
		if(elements[i].isValid==1 && elements[i].delayMs<response){
			response = elements[i].delayMs;
		}
	}
	return response;
}

//=====================================================================

void EventQueue::executeAllEvents(unsigned int timeSinceLastMs){
  for(int i = 0; i<queueSize; ++i){
     //only execute valid events with an expired event time
     if(elements[i].isValid==1){ 
       if(elements[i].delayMs <=0){
         elements[i].function(elements[i].values);
         elements[i].isValid = 0; 
       }else{
          //decrement the wait time for all other valid events
         elements[i].delayMs = elements[i].delayMs - timeSinceLastMs;
         if(elements[i].delayMs < 0){
           elements[i].delayMs=0;
         }
       }//endif-else
     }//endif valid      
  }//endfor
}

bool EventQueue::isEmpty(){
	for(int i = 0; i<queueSize; ++i){
		if(elements[i].isValid==1){
			return false;
		}
	}
	return true;
}

void EventQueue::clear(){
	for(int i = 0; i<queueSize; ++i){
		elements[i].isValid=0;
	}
}