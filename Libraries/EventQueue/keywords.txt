#######################################
# Syntax Coloring Map EventQueue
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

EventQueue KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################

initEvents  KEYWORD2
addQueueEvent  KEYWORD2
executeFirstValidEvent  KEYWORD2
executeAllEvents  KEYWORD2