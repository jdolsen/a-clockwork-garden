#ifndef EVENT_QUEUE_h
#define EVENT_QUEUE_h

#include <inttypes.h>
#include <Arduino.h> 
#include <stdarg.h>
#define NUM_QUEUE_ELEMENT_VALUES 3


typedef struct queueelementstruct {
  void (*function)(unsigned int values[]);
  unsigned int values[NUM_QUEUE_ELEMENT_VALUES];
  unsigned int delayMs;
  byte isValid;
} QueueElement;

typedef void (*QueueFunction)(unsigned int values[]);


class EventQueue{

	public: 
	int queueSize;
	QueueElement* elements;
	bool* queueingEnabled;
	//int tickTimeMs;

	public:
	EventQueue();
	void initEvents(QueueElement* array, const int& size,bool* queueEnable);//, const int& clockTick);
	bool addQueueEvent(QueueFunction fun, unsigned int delayMs, int n_args, ...);
	bool executeFirstValidEvent(unsigned int timeSinceLastMs);
	unsigned int getTimeUntilNextEvent();
	void executeAllEvents(unsigned int timeSinceLastMs);
	bool isEmpty();
	void clear();
};

#endif