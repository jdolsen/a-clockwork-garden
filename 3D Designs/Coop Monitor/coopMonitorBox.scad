wallT = 2;
boardX = 45;
boardY= 52;
keypadX = 70;
keypadY = 70;
keypadConnectX=28;
keypadConnectY=5;
keypadConnectOffsetX=24+5;
keypadConnectOffsetY=5;
boxMargin = 10;
boxX = 2*wallT+boardX+keypadX+boxMargin;
boxY = keypadY+boxMargin;
boxH = 36;
displayMargin=3;
insetDepth=2;

echo(boxX);
echo(boxY);

//box Top

difference(){
    cube([boxX,boxY,boxH/2]);
    translate([wallT,wallT,wallT+.5]){cube([boxX-2*wallT,boxY-2*wallT,boxH]);}
    translate([boxMargin/2+keypadX/2-keypadConnectX/2,keypadConnectOffsetY,-1]){
        cube([keypadConnectX,keypadConnectY,2*boxH]);}
        
    //#translate([5,5,boxH-4]){cube(keypadX,keypadY,5);}
    //#translate([boxX-boxMargin/2-boardX, boxY-boardY-boxMargin/2, boxH-4]){cube([boardX,boardY,5]);}
    translate([displayMargin/2+boxX-boxMargin/2-boardX, displayMargin/2+boxY-boardY-boxMargin/2, -1]){
        cube([boardX-displayMargin,boardY-displayMargin,2*boxH]);}
    translate([wallT/2, wallT/2, boxH/2-insetDepth]){cube([boxX-wallT,boxY-wallT,boxH]);}    
}

//box bottom
/*
translate([0,boxY+10,0]){
    difference(){
    cube([boxX,boxY,boxH/2]);
    translate([wallT,wallT,wallT+.5]){cube([boxX-2*wallT,boxY-2*wallT,boxH]);}
    translate([wallT+3+5,0,boxH/4]){rotate(90,[1,0,0]){cylinder(r=3,h=10,center=true);}}
    
    translate([-1,-1,boxH/2-insetDepth]){cube([boxX+10,1+wallT/2,boxH]);}
    translate([-1,boxY-wallT/2,boxH/2-insetDepth]){cube([boxX+10,1+wallT/2,boxH]);}
    
    translate([boxX-wallT/2,-1,boxH/2-insetDepth]){cube([1+wallT/2,boxY+10,boxH]);}
    translate([-1,-1,boxH/2-insetDepth]){cube([1+wallT/2,boxY+10,boxH]);}
}  
}
*/
