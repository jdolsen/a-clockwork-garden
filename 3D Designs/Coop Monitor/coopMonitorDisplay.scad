

wallT = 1.5;
boardX = 45;
boardY = 44.5;
boardT = 6;
boardExt = 50;
clipExt = 1;
openX = 32;
openY = 21;
openOffsetX = 9.5+wallT;
openOffsetY = 6.5+wallT;
root3=sqrt(3);
echo(boardX*2/root3);


difference(){
    cube([boardX+2*wallT, boardY+2*wallT, wallT+boardT+boardExt]);
    translate([wallT,wallT,wallT]){cube([boardX, boardY, wallT+boardT+boardExt+1]);}
    translate([openOffsetY,openOffsetX,-1]){cube([openX, openY, wallT+boardT]);}   
    
    //bevel the extension
    translate([-1,-1,wallT+boardT]){rotate(30,[1,0,0]){cube([300,300,300]);}}
}

translate([0,boardY/2,wallT+boardT]){cube([wallT++clipExt,7,1.25]);}
translate([boardX+wallT-clipExt,boardY/2,wallT+boardT]){cube([wallT++clipExt,7,1.25]);}